package statistics;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;

import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multiset.Entry;
import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;

import english.English;

public class MonoFrequencies
{
	private TreeMultimap<Double, Character> sorted = TreeMultimap.create(Collections.reverseOrder(), Ordering.natural());
	private Map<Character, Double> percentages = new HashMap<>();
	
	private MonoFrequencies(){}
	
	public static MonoFrequencies of(CharSequence text)
	{
		MonoFrequencies result = new MonoFrequencies();
		Multiset<Character> counts = English.charFrequencies(text);
		for(Entry<Character> c : counts.entrySet())
		{
			result.percentages.put(c.getElement(), c.getCount() * (1.0 / text.length()));
		}
		Map<Character, Double> data = result.percentages;
		for(Map.Entry<Character, Double> e : data.entrySet())
		{
			result.sorted.put(e.getValue(), e.getKey());
		}
		return result;
	}
	
	public double frequencyOf(char c)
	{
		return percentages.get(c);
	}
	
	public Iterable<Map.Entry<Double, Character>> frequenciesDescending()
	{
		return sorted.entries();
	}
	
	public NavigableMap<Double, Set<Character>> frequencies()
	{
		NavigableMap m = sorted.asMap();
		return (NavigableMap<Double, Set<Character>>) m;
	}
	
	public Multimap<Double, Character> data()
	{
		return sorted;
	}
	
	public String toString()
	{
		return frequenciesDescending().toString();
	}

	public static MonoFrequencies ofEnglish()
	{
		MonoFrequencies result = new MonoFrequencies();
		result.sorted = English.englishMonographs();
		result.percentages = English.englishPercentages();
		return result;
	}
	
	
}
