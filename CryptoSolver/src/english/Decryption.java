package english;


/**
 * A possible decryption result, with partial or full plaintext, an expected confidence, and a % decrypted.
 * Also includes an optional description of the decryption used.
 * @author Rajan
 *
 */
public class Decryption
{
	public String ciphertext;
	public String plaintext;
	public double confidence;
	public double completion;
	public String description;
	
	
}
