package english;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public final class Quadgraph
{
	public final char a, b, c, d;

	private Quadgraph(char a, char b, char c, char d)
	{
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}
	
	

	public static Quadgraph of(char a, char b, char c, char d)
	{
		return new Quadgraph(a, b, c, d);
	}
	
	public static Quadgraph at(String text, int index)
	{
		return Quadgraph.of(text.charAt(index), text.charAt(index + 1), text.charAt(index + 2), text.charAt(index + 3));
	}
	
	public static Quadgraph of(String text)
	{
		return at(text, 0);
	}
	
	public static Quadgraph of(char[] text)
	{
		return at(text, 0);
	}
	
	public static Map<Quadgraph, Double> frequenciesIn(String text)
	{
		Map<Quadgraph, Double> results = new LinkedHashMap<>();
		for(int i = 0; i < text.length() - 3; i++)
		{
			results.merge(at(text, i), 1.0, ((a, b) -> (a + b)));
		}
		int numQuads = text.length() - 3;
		results.replaceAll((q, d) -> d / numQuads);
		return results;
	}
	
	public static Map<Quadgraph, Double> frequenciesIn(char[] text)
	{
		Map<Quadgraph, Double> results = new LinkedHashMap<>();
		for(int i = 0; i < text.length - 3; i++)
		{
			results.merge(at(text, i), 1.0, ((a, b) -> (a + b)));
		}
		int numQuads = text.length - 3;
		results.replaceAll((q, d) -> d / numQuads);
		return results;
	}


	public static Quadgraph at(char[] text, int index)
	{
		return Quadgraph.of(text[index], text[index + 1], text[index + 2], text[index + 3]);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + a;
		result = prime * result + b;
		result = prime * result + c;
		result = prime * result + d;
		return result;
	}
	
	public long toLong()
	{
		return LongQuads.of(this);
	}

	@Override
	public boolean equals(Object obj)
	{
		Quadgraph other = (Quadgraph) obj;
		return  (a == other.a) && (b == other.b) && 
				(c == other.c) && (d == other.d);
	}
	
	public String toString()
	{
		return "" + a + b + c + d;
	}
}
