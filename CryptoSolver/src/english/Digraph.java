package english;

import net.openhft.koloboke.collect.map.hash.HashIntObjMap;
import net.openhft.koloboke.collect.map.hash.HashIntObjMaps;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

/**
 * Represents two english characters, immutable. Instances are garunteed to be cached, obtain one from Digraph.of
 * Equality can be tested using == instead of equals() due to the cached nature of the objects.
 * @author Rajan
 *
 */
public final class Digraph
{
	public final char first, second;
	
	private Digraph(char a, char b)
	{
		first = a;
		second = b;
	}
	
	private Digraph(int d)
	{
		first = (char)(d >> 16);
		second = (char) d;
	}
	
	private static HashIntObjMap<Digraph> intDigraphs = HashIntObjMaps.newUpdatableMap(26 * 26);
	
	
	public static Digraph of(char a, char b)
	{
		return intDigraphs.computeIfAbsent(IntDigraphs.of(a, b), (i) -> new Digraph(a, b));
	}
}
