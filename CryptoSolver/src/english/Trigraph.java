package english;

/**
 * Represents 3 english characters, immutable.
 * @author Rajan
 *
 */
public class Trigraph
{
	public final char a, b, c;
	
	private Trigraph(char a, char b, char c)
	{
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public static Trigraph of(char a, char b, char c)
	{
		return new Trigraph(a, b, c);
	}
}
