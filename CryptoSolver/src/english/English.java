package english;


import static utils.Utils.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


















import net.openhft.koloboke.collect.map.LongDoubleCursor;
import net.openhft.koloboke.collect.map.LongDoubleMap;
import net.openhft.koloboke.collect.map.LongIntCursor;
import net.openhft.koloboke.collect.map.LongIntMap;
import net.openhft.koloboke.collect.map.hash.HashLongDoubleMap;
import net.openhft.koloboke.collect.map.hash.HashLongDoubleMaps;
import net.openhft.koloboke.collect.set.hash.HashCharSet;
import net.openhft.koloboke.collect.set.hash.HashCharSets;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multiset.Entry;
import com.google.common.collect.Ordering;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.TreeMultimap;

/**
 * Provides static methods and constants related to the english language, for use in cryptanalysis of english text.
 * @author Rajan
 *
 */
public class English
{
	public static final String alphabet = "abcdefghijklmnopqrstuvwxyz";
	public static final String uppercase = alphabet.toUpperCase();
	public static final HashCharSet alphaset = genAlphaset();
	
	public static final int minWordLength = 4;
	private static TreeMultimap<Double, Character> sortedEnglishFrequencies = buildFrequencies();
	
	private static Map<Character, Double> englishFrequencies = buildPercentages();
	

	public static Multiset<Character> charFrequencies(CharSequence text)
	{
		Multiset<Character> results = HashMultiset.create(26);
		for(int i = 0; i < text.length(); i++)
		{
			results.add(text.charAt(i));
		}
		return results;
	}
	
	private static HashCharSet genAlphaset()
	{
		return HashCharSets.newImmutableSet(alphabet.toCharArray());
	}

	private static ImmutableMap<Character, Integer> buildCharValues()
	{
		ImmutableMap.Builder<Character, Integer> b = ImmutableMap.builder();
		for(int i = 0; i < alphabet.length(); i++)
		{
			b.put(alphabet.charAt(i), i);
			b.put(uppercase.charAt(i), i);
		}
		return b.build();
	}

	private static Map<Character, Double> buildPercentages()
	{
		try (Scanner s = new Scanner(new File("monographs.txt")))
		{
			Map<Character, Double> results = new HashMap<>();
			int i = 0;
			while(s.hasNextDouble())
			{
				results.put( alphabet.charAt(i), s.nextDouble());
				i++;
			}
			return results;
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private static TreeMultimap<Double, Character> buildFrequencies()
	{
		
		try (Scanner s = new Scanner(new File("monographs.txt")))
		{
			TreeMultimap<Double, Character> results = TreeMultimap.create(Collections.reverseOrder(), Ordering.natural());
			int i = 0;
			while(s.hasNextDouble())
			{
				results.put(s.nextDouble(), uppercase.charAt(i));
				i++;
			}
			return results;
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			return null;
		}
		
	}

	public static Map<Character, Double> charPercentages(CharSequence text)
	{
		Map<Character, Double> results = new HashMap<>(26);
		Multiset<Character> counts = charFrequencies(text);
		for(Entry<Character> c : counts.entrySet())
		{
			results.put(c.getElement(), c.getCount() * (1.0 / text.length()));
		}
		return results;
	}
	
	public static TreeMultimap<Double, Character> percentageOfMonographs(CharSequence text)
	{
		TreeMultimap<Double, Character> results = TreeMultimap.create();
		Map<Character, Double> data = charPercentages(text);
		for(Map.Entry<Character, Double> e : data.entrySet())
		{
			results.put(e.getValue(), e.getKey());
		}
		return results;
	}
	
	
	public Multiset<Digraph> digraphFrequences(CharSequence text)
	{
		Multiset<Digraph> results = HashMultiset.create(26 * 26);
		for(int i = 0; i < text.length() - 1; i += 2)
		{
			results.add(Digraph.of(text.charAt(i), text.charAt(i + 1)));
		}
		return results;
	}
	
	public static TreeMultimap<Double, Character> englishMonographs()
	{
		return sortedEnglishFrequencies;
	}
	
	public static double englishness(CharSequence text)
	{
		return Double.NaN; // Perform various tests and guess how close to english this text is.
	}
	
	public static double monographEnglishness(CharSequence text)
	{
		return Double.NaN; //Compare monograph frequencies in text with english frequencies, report similarity
	}
	/**
	 * Map from quadgraphs to their frequency in english. Ordered from most frequent to least frequent.
	 */
	public static final ImmutableMap<Quadgraph, Double> quadFrequencies = loadQuadgraphs();
	
	
	public static final ImmutableMap<Quadgraph, Double> logFrequencies = logQuads();
	
	
	public static double logScore(String text)
	{
		return logQuadScore(Quadgraph.frequenciesIn(text));
	}
	
	public static double logScore(char[] text)
	{
		double score = 0;
		long quadgraph = LongQuads.of(text[0], text[1], text[2], text[3]);
		for(int i = 4; i < text.length; i++)
		{
			score += longLogQuads.getOrDefault(quadgraph, ALIEN_SCORE);
			quadgraph = (quadgraph << 16) | text[i];
		}
		score += longLogQuads.getOrDefault(quadgraph, ALIEN_SCORE);
		return score / (text.length - 3);
	}
	
	private static double logQuadScore(Map<Quadgraph, Double> frequencies)
	{
		double score = 0;
		for(Map.Entry<Quadgraph, Double> e : frequencies.entrySet())
		{
			double expected = logFrequencies.getOrDefault(e.getKey(), ALIEN_SCORE);
			score += expected * e.getValue();
		}
		return score;
	}
	
	public static double quadgraphScore(String text)
	{
		return score(Quadgraph.frequenciesIn(text));
	}
	
	public static double quadgraphScore(char[] text)
	{
		return score(Quadgraph.frequenciesIn(text));
	}
	
	private static double score(Map<Quadgraph, Double> frequencies)
	{
		double score = 0;
		for(Map.Entry<Quadgraph, Double> e : frequencies.entrySet())
		{
			double expected = quadFrequencies.getOrDefault(e.getKey(), 0.0);
			score += expected * e.getValue();
		}
		return score;
	}
	static final HashLongDoubleMap longLogQuads = buildLongLogQuads();
	private static final double ALIEN_SCORE = -1 / Math.E;
	private static double score(LongDoubleMap frequencies)
	{
		LongDoubleCursor c = frequencies.cursor();
		double score = 0;
		while(c.moveNext())
		{
			double expected = longLogQuads.getOrDefault(c.key(), ALIEN_SCORE);
			score += expected * c.value();
		} 
		return score;
	}
	
	private static double score(LongIntMap counts, int total)
	{
		LongIntCursor c = counts.cursor();
		double score = 0;
		while(c.moveNext())
		{
			double expected = longLogQuads.getOrDefault(c.key(), ALIEN_SCORE);
			score += expected * c.value();
		} 
		return score / total;
	}
	
	private static HashLongDoubleMap buildLongLogQuads()
	{
		HashLongDoubleMap result = HashLongDoubleMaps.newUpdatableMap(logFrequencies.size());
		logFrequencies.forEach((Quadgraph q, Double d) -> 
		{
			result.put(LongQuads.of(q), d.doubleValue());
		});
		return result;
	}
	private static long numFileGrams = 0;
	private static ImmutableMap<Quadgraph, Double> loadQuadgraphs()
	{
		numFileGrams = 0;
		ImmutableMap.Builder<Quadgraph, Double> builder = ImmutableMap.builder();
		Scanner file;
		try
		{
			file = new Scanner(new File("gutgraphs.txt"));
		}
		catch (FileNotFoundException e)
		{
			throw new RuntimeException(e);
		}
		numFileGrams = file.nextLong();
		while(file.hasNext())
		{
			Quadgraph q = Quadgraph.of(file.next());
			int value = file.nextInt();
			builder.put(q, value * (1.0 / numFileGrams));
		}
		file.close();
		return builder.build();
	}
	
	private static ImmutableMap<Quadgraph, Double> logQuads()
	{
		numFileGrams = 0;
		ImmutableMap.Builder<Quadgraph, Double> builder = ImmutableMap.builder();
		Scanner file;
		try
		{
			file = new Scanner(new File("gutgraphs.txt"));
		}
		catch (FileNotFoundException e)
		{
			throw new RuntimeException(e);
		}
		numFileGrams = file.nextLong();
		while(file.hasNext())
		{
			Quadgraph q = Quadgraph.of(file.next());
			int value = file.nextInt();
			builder.put(q, Math.log(value));
		}
		file.close();
		return builder.build();
		
	}

	public static final ImmutableSet<String> dictionary = buildDictionary();
	
	public static double wordFrequency(String text)
	{
		int wordLetters = 0;
		for(int i = 0; i < text.length(); i++)
		{
			for(int j = 10; j >= minWordLength; j--)
			{
				if(i + j > text.length()) continue;
				if(dictionary.contains(text.substring(i, i + j)))
				{
					i += j - 1;
					wordLetters += j;
					j = 0;//End inner loop
					continue;
				}
			}
		}
		return wordLetters * (1.0 / text.length()); // Find english words and report what fraction of the text is contained by english words. Expensive compared to frequency checks.
	}


	private static ImmutableSet<String> buildDictionary()
	{
		Iterator<String> elements;
		try
		{
			elements = new Scanner(new File("allWords.txt"));
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			return null;
		} // Get iterator linked to dictionary file
		Stream<String> words = StreamSupport.stream(Spliterators.spliteratorUnknownSize(elements, 0), false).map(String::toLowerCase);
		return ImmutableSet.copyOf(words.iterator());
	}

	public static Map<Character, Double> englishPercentages()
	{
		return englishFrequencies;
	}
	
	private static final SetMultimap<String, String> wordsContaining = HashMultimap.create();
	
	public static Set<String> wordsContaining(String fragment)
	{
		Set<String> stored = wordsContaining.get(fragment);
		if(stored.isEmpty()) 
		{
			for(String w : dictionary)
			{
				if(w.contains(fragment)) stored.add(w);
			}
		}
		return Collections.unmodifiableSet(stored);
	}
	
	public static void main(String[] args)
	{
		String text = "charlienoneedtoapologiselifewasgettingdullbehindadeskandiwasgladtohaveanexcusetoflybacktoeuropeiamintriguedaboutthereichsdoktorihadntcomeacrossthisbeforewhendidyoufirstcomehearofitithinkimayalreadybemakingsomeprogressonarrivalifoundapostcardwaitingformeonthematattheembassywithnomessageonitatleastthatswhatitlookedlikeatfirstididnoticethatthelettersonthefrontcouldbehighlightedtopickoutthephrasethereichsdoktorsounlessthatisanextraordinarycoincidenceifigureditmustberelatedtoourinvestigationthestrangestthingwasthatthepostcardhadastampbutnopostmarkonitsoitcanthavebeenpostedsinceitwasntsignediassumetheywantedtostayanonymousandicouldntseewhytheywouldhavetakentheriskofhanddeliveringitbutintheendiworkeditouttherewasahiddenmessageillleaveittoyoutofigureoutwhereitwashiddenanywayiveattachedthemessageifoundiknowthatrelationswiththreeofthefourpowersarerelativelystablebutithinkweneedtokeepthistoourselvesfornowallthebestharry";
		System.out.println(addSpaces(text));
	}
	
	public static String addSpaces(String input)
	{
		StringBuilder result = new StringBuilder();
		for(int i = 0; i < input.length(); i++)
		{
			for(int j = 0; j < 16 && i + j <= input.length(); j++)
			{
				String stuff = input.substring(i, i + j);
				if(dictionary.contains(stuff))
				{
					result.append(stuff);
					result.append(' ');
					i += stuff.length();
					break;
				}
			}
			result.append(input.charAt(i));
		}
		
		return result.toString();
	}
}
