package english;

public class IntDigraphs
{
	public static int of(char a, char b)
	{
		return (((int) a) << 16) | b;
	}
}
