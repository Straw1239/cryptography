package english;
import java.util.Arrays;

import static english.English.*;
import utils.CharIterator;
import utils.Utils;
import net.openhft.koloboke.collect.map.hash.HashLongDoubleMap;
import net.openhft.koloboke.collect.map.hash.HashLongDoubleMaps;
import net.openhft.koloboke.collect.map.hash.HashLongIntMap;
import net.openhft.koloboke.collect.map.hash.HashLongIntMaps;
public class LongQuads
{
	private LongQuads(){}
	
	public static long of(char a, char b, char c, char d)
	{	
		return (((long) a) << 48) | (((long) b) << 32) | (((long) c) << 16) | ((long) d);
	}
	
	public static long of(Quadgraph q)
	{
		return of(q.a, q.b, q.c, q.d);
	}
	
	
	public static HashLongDoubleMap frequenciesIn(char[] text)
	{
		HashLongDoubleMap result = HashLongDoubleMaps.newUpdatableMap();
		long quadgraph = of(text[0], text[1], text[2], text[3]);
		for(int i = 4; i < text.length; i++)
		{
			result.compute(quadgraph, (k, v) -> v + 1);
			quadgraph = (quadgraph << 16) | text[i];
		}
		result.compute(quadgraph, (k, v) -> v + 1);
		result.replaceAll((long k, double v) -> v / (text.length - 3));
		return result;
	}
	
	public static HashLongIntMap countGraphs(CharIterator source)
	{
		HashLongIntMap result = HashLongIntMaps.getDefaultFactory().withDefaultValue(0).newUpdatableMap();
		long quadgraph = of(source.next(), source.next(), source.next(), source.next());
		while(source.advance())
		{
			result.compute(quadgraph, (k, v) -> v + 1);
			quadgraph = (quadgraph << 16) | source.get();
		}
		result.compute(quadgraph, (k, v) -> v + 1);
		return result;
	}
	private static char nextAlpha(CharIterator source)
	{
		char c = 0;
		while(!Utils.isAlphabetic(c))
		{
			source.advance();
			c = source.get();
		}
		return c;
	}
	public static HashLongIntMap countAlphaGraphs(CharIterator source)
	{
		HashLongIntMap result = HashLongIntMaps.getDefaultFactory().withDefaultValue(0).newUpdatableMap();
		long quadgraph = of(nextAlpha(source), nextAlpha(source), nextAlpha(source), nextAlpha(source));
		int total = 0;
		while(source.advance())
		{
			char c = source.get();
			if(!Utils.isAlphabetic(c)) continue;
			total++;
			//System.out.println(toQuadgraph(quadgraph));
			result.compute(quadgraph, (k, v) -> v + 1);
			quadgraph = (quadgraph << 16) | c;
		}
		total++;
		result.compute(quadgraph, (k, v) -> v + 1);
		result.put(0, total);
		return result;
	}
	
	public static HashLongIntMap countGraphs(char[] text)
	{
		HashLongIntMap result = HashLongIntMaps.newUpdatableMap();
		long quadgraph = of(text[0], text[1], text[2], text[3]);
		for(int i = 4; i < text.length; i++)
		{
			result.compute(quadgraph, (k, v) -> v + 1);
			quadgraph = (quadgraph << 16) | text[i];
		}
		result.compute(quadgraph, (k, v) -> v + 1);
		return result;
	}

	public static Quadgraph toQuadgraph(long k)
	{
		return Quadgraph.of((char)(k >> 48), (char)(k >> 32), (char)(k >> 16), (char)k);
	}
	
	public static String toString(long quad)
	{
		return toQuadgraph(quad).toString();
	}
	
	public static char first(long quad)
	{
		return (char) (quad >> 48);
	}
	
	public static char second(long quad)
	{
		return (char) (quad >> 32);
	}
	
	public static char third(long quad)
	{
		return (char) (quad >> 16);
	}
	
	public static char fourth(long quad)
	{
		return (char) (quad);
	}
	
	public static boolean isAlphabetic(long quad)
	{
		return alphaset.contains(first(quad)) && alphaset.contains(second(quad)) && alphaset.contains(third(quad)) && alphaset.contains(fourth(quad));
	}
	
	
}
