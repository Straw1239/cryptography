package specialized.solitaire;

import java.util.Arrays;
import java.util.Collections;

import utils.ByteArraySwapper;
import utils.IntPair;
import utils.TabuCache;
import utils.Utils;
import english.English;

public class Solver
{
	

	
	
	static byte[] deck = {8, 29, 19, 32, 50, 45, 11, 10, 12, 2, 25, 49, 48, 46, 31, 21, 22, 26, 28, 23, 37, 51, 9, 3, 24, 18, 36, 53, 15, 33, 6, 1, 44, 27, 7, 41, 14, 43, 34, 38, 40, 30, 16, 53, 13, 20, 52, 17, 42, 5, 4, 47, 35, 39};
	static int[] unknowns = {2, 4,6,7,8,9,10,11,13,14,15,16,17,18,19,20};
	static byte[] unknownVals = {19,2,28,21,12,22,11,10,8,45,50,8,26,31,48,49};
	static int JOKERA = -1, JOKERB = -1;
	final static boolean swapJokers = false;
	final static boolean reverseDeck = true;
	static
	{
		if(reverseDeck)
		{
			for(int i = 0; i < deck.length / 2; i++)
			{
			    byte temp = deck[i];
			    deck[i] = deck[deck.length - i - 1];
			    deck[deck.length - i - 1] = temp;
			}
		}
		for(int i = 0; i < deck.length; i++)
		{
			if(deck[i] == 53)
			{
				if(JOKERA < 0) JOKERA = i;
				else if(JOKERB < 0) JOKERB = i;
			}
		}
		if(swapJokers)
		{
			int temp = JOKERA;
			JOKERA = JOKERB;
			JOKERB = temp;
		}
		byte[] newDeck = new byte[deck.length + 2];
		System.arraycopy(deck, 0, newDeck, 2, deck.length);
		deck = newDeck;
		
	}
	static String ciphertext = "IUTWMVVHVRORNXZZAGPPJSLVPFDLVZMEVGJIVYDZPNAPKQXCIZLGRZWNNCSVKPQTMLKPQNWPGOAYVAPQIPQWRMIXPBTCCEGWHLOZQRFYZGHEJCFETFRULYBLUDNHNYGBEKBKSNXYMRCTHNLXHKHKDFCBBWGVJQBLIESQAJWVLZQTLLASRESDVJMRTBJDOVOAJPQQIVYZHFAFQBHGMVOSDEXBYHKSPYSQLDFRZFYJHEDWPZMVBDCRIYALMMVQWJHVIPDUCKFVZBXMDQVBMXOKODOGYEBWLACFMUVQNSQRKMMNWZBEOOUEXIYDJWUJICKRFQLCESIKHCJQRUFPRRGYHSTZNWOSPAZULTCZPRSOYVETXLLAQMJMVUSOPLCEWYLJUADSJGTLOHOXRHSPZGLADFFAHORATZOBMRVVFDWANXPGUNQGLIHGTRMWBJMFTALCCPZGVLCSINPBUWOVPYXUQUZKTGLTVRZLIRRFNWUULJIVFMEVFIPUNCMDUZAGRHDACDPFKTHTKDKOGUSPYENXTPQUROUZTWCMIGPCKLYWZGUWJRLVKNKQKGPDQNABIRLPVKHOMWXWCQTLGWHXBJFYGIVIWWJXWAVUCOFKJUMZKXKKOEGOETLRAKQVMGKOABGIXQLQMBJYBJUOIZRWHKZCDSMNHMWTRSDBSRULECERARYGFDPERECYGULSJCJWVMLNRZXKQRTTSZWJUVSUXLGKMQPHJWAUBUEJXKYAXCEBLGJHTNKTNZSGYLOFUZUTLNYBHBKGKSCDWIYUXXFYJPNTAFKBGCNLJVGTKDCNBHUSAZRBWSXKICXDISPIRYEOIVXZAWAZIFPGAUYDQSWYCSIQDENTCTAONTOBCIVFYPVPDEMUHDPNTSUOSWVMLXECSHCMHESCGWSUSTAYHKUMOXUFEANTUHNDZHZFLRSHCZBASXPPMCNWMSJTANASBRPHDWJUCTTGMHNTTPIGTVJWWNFUWEOZMCIQMDZDJGLKSSYOXIBGIHPZOMNYBORFNCBTNUHQDOPUOFCCLDFUHIPMKCZKCZVZNMFLWOKIZFKINWQNROAZYLCTMUZYUGOUIMEQSQQAAIQVYQRSPZPUXUNBAORFDDASVMADOGRNPBPKNXGXQOKSEHEAJNZNMQIUMPLHWUFWLEOBKPIASZALJPZQUIKJSGKPGEGMPFBUNHOFKXTSCJMTYBUJEBYNNEVQHKNTIUJBJEEUSQOINRDAZUQMEWEELBLBSGUGXDXLWTUEODCKZYXJUODPPGBSPLAKHPKUZYVWGXMVXEAENQYBPKSDJMTZIBEYMTOFWCVOYZLJSKXGBKAHDTZAMZSFPGPYFFWRBHLNXOAXOITZVFBEXAKVYPAYTIRZMRKIYZRKIQNSDOINPTWMACVOJCXWCOXCEAJBQULUYWQLRERSUIIQTBASGUMAORADTIWOIDHEWLYZBADGFMHHWXNQCZKFTBVJRSYMKGTMLRGNHPUZYOVAOGTVHKHHEQBKTHJYBCUONPEUPDPJMLEOZILYNABGMPEEVJHKADCUEHMNEFWJURTJKTBKZSMTKYPCRVGFPHEIDVFSVNFUMSYAXJAVGMDSZRMHMQVSUEKUWFZFRYOROKWORNQUNJXBHNZAYXWWBEISHIQBOJAAYEKWMGJLGHFDRKBEJTQUQKVRHNJGFHARSOXBRZHKTJFJFNRXQZQRMFKNXRWLVCZBZSFQAOCLPZSGIOTMXTQHBHVYVRYIUSKFXFPKNSQITSRMYGRYXWRFQMBBMJTYOCDTTW";
	static char[] cipher = ciphertext.toCharArray();
	public static void main(String[] args)
	{
//		System.out.println(ciphertext.length());
//		List<Byte> vals = new ArrayList<>();
//		for(byte i : unknownVals)
//		{
//			vals.add(i);
//		}
//		Collection<List<Byte>> p = Collections2.permutations(vals);
//		double bestScore = Double.NEGATIVE_INFINITY;
//		for(List<Byte> order : p)
//		{
//			for(int i = 0; i < unknownVals.length; i++)
//			{
//				deck[unknowns[i]] = order.get(i);
//			}
//			double score = score(deck);
//			if(score > bestScore)
//			{
//				System.out.println(Arrays.toString(deck));
//				System.out.println(Solitaire.decrypt(cipher, Solitaire.keyStream(deck, 1770, 43, 27)));
//				System.out.println(score);
//				bestScore = score;
//			}
//		}
		tabuSolve(ciphertext);
	}
	private static double score(byte[] deck, char[] cipher)
	{
		return English.logScore(Solitaire.decrypt(cipher, Solitaire.keyStream(deck, chars, JOKERA, JOKERB)));
	}
	static int chars = 10;
	
	private static void tabuSolve(String text)
	{
		double bestScore = Double.NEGATIVE_INFINITY;
		byte[] bestKey = unknownVals;
		byte[] key = unknownVals.clone();
		Collections.shuffle(Arrays.asList(key));
		char[] cipher = text.toCharArray();
		TabuCache<IntPair> noSwaps = new TabuCache<>(45);
		TabuCache<Integer> noMove = new TabuCache<>(6);
		while(true)
		{
			ByteArraySwapper gen = new ByteArraySwapper(key);
			double bestNeighborScore = Double.NEGATIVE_INFINITY;
			int index1 = -1, index2 = -1;
			while(gen.hasNext())
			{
				gen.advance();
				double score = score(getDeck(gen.get()), cipher);
				if(score > bestScore)
				{
					bestScore = score;
					bestKey = gen.get().clone();
					bestNeighborScore = score;
					System.out.println(Arrays.toString(getDeck(bestKey)));
					System.out.println(Solitaire.decrypt(cipher, Solitaire.keyStream(getDeck(bestKey), chars, JOKERA, JOKERB)));
					System.out.println(score);
					index1 = gen.firstSwapIndex();
					index2 = gen.secondSwapIndex();
				}
				else
				{
					if(noSwaps.allow(IntPair.of(gen.firstSwapIndex(), gen.secondSwapIndex())) && noMove.allow(gen.firstSwapIndex()) && noMove.allow(gen.secondSwapIndex()))
					{
						if(score > bestNeighborScore)
						{
							bestNeighborScore = score;
							index1 = gen.firstSwapIndex();
							index2 = gen.secondSwapIndex();
						}
					}
				}
			
			}
			gen.revert();
			Utils.swap(key, index1, index2);
			noSwaps.add(IntPair.of(index1, index2));
			noMove.add(index1);
			noMove.add(index2);
			
			
			
		}
		
	}
	
	private static byte[] getDeck(byte[] key)
	{
		for(int i = 0; i < key.length; i++)
		{
			deck[unknowns[i]] = key[i];
		}
		return deck;
	}
	
	
	
}
