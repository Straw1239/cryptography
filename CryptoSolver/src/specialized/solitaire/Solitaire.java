package specialized.solitaire;

import java.util.Arrays;

import english.English;
import net.openhft.koloboke.function.IntToByteFunction;
import static java.lang.Math.*;
public class Solitaire
{
	public static void main(String[] args)
	{
		byte[] key = new byte[56];
		for(int i = 0; i < 52; i++)
		{
			key[i + 2] = (byte)(i + 1);
		}
		key[54] = JOKERA;
		key[55] = JOKERB;
		System.out.println(keyStream(key, 10, 52, 53));
		char[] plaintext = "AAAAAAAAAA".toCharArray();
		System.out.println(decrypt(encrypt(plaintext, keyStream(key, 10, 52, 53)), keyStream(key, 10, 52, 53)));
	}
	
	private static final int SHIFT = 2;
	private static final byte JOKERA = 53;
	private static final byte JOKERB = 53;
	private static final int LAST = 53;
	public static char[] keyStream(byte[] key, int length, int jokerA, int jokerB)
	{
		//if(key.length != 54) throw new IllegalArgumentException();
		class Gen
		{
			int start;
			
			void jokerStepA()
			{
//				
				if(jA != LAST) 
				{
					deck[start + jA] = deck[start + jA + 1]; 
					jA++; 
				}
				else
				{
					start--;
					jB++;
					deck[start] = deck[start + 1];
					jA = 1;
				}
				if(jA == jB) jB--;
			}
			
			void jokerStepB()
			{
				if(jB != LAST) 
				{
					deck[start + jB] = deck[start + jB + 1]; 
					jB++; 
				}
				else
				{
					start--;
					jA++;
					deck[start] = deck[start + 1];
					jB = 1;
				}
				if(jB == jA) jA--;
			}
			
			byte[] deck = Arrays.copyOf(key, key.length);
			byte[] copy = new byte[deck.length];
			int jA = jokerA, jB = jokerB;
			char getChar()
			{
				// Step both jokers
				start = 2;
				
				jokerStepA();
				jokerStepB();
				jokerStepB();
				
				
				
				
				deck[start + jA] = JOKERA;
				deck[start + jB] = JOKERB;
				

				int j1 = min(jA, jB);
				int j2 = max(jA, jB);
				// Triple cut
				
				System.arraycopy(deck, start, 			copy, 54 - j1, 			j1);
				System.arraycopy(deck, start + j1, 		copy, LAST - j2, 	j2 - j1 + 1);
				System.arraycopy(deck, start + j2 + 1, 	copy, 0, LAST - j2);
				//byte[] temp = deck;
				start = 2;
				int jShift = LAST - j2 - j1;
				jA += jShift;
				jB += jShift;

				// Count cut
				int v = copy[LAST];
				System.arraycopy(copy, v , deck, start, LAST - v );
				System.arraycopy(copy, 0, deck, start + LAST - v , v);
				deck[LAST + start] = copy[LAST];
				if(jA < v)
				{
					jA += 53 - v;
				} 
				else if(jA < LAST)
				{
					jA -= v;
				}
				if(jB < v)
				{
					jB += 53 - v;
				} 
				else if(jB < LAST)
				{
					jB -= v;
				}
				
				
				
				//Output card
				v = deck[start];
				byte card = deck[v + start];
				//System.out.println(card);
				return toLetter(card);
			};
		}
		Gen g = new Gen();
		char[] out = new char[length];
		int index = 0;
		while(index < length)
		{
			char next = g.getChar();
			if(next == '\0') continue;
			out[index] = next;
			index++;
		}
		return out;
	}
	
	private static char toLetter(byte card)
	{
		if(card >= JOKERA) return '\0';
		card--;
		return (char)((card % 26) + 65);
	}
	
	public static char[] encrypt(char[] plaintext, char[] keyStream)
	{
		char[] result = new char[plaintext.length];
		for(int i = 0; i < result.length; i++)
		{
			result[i] = (char)( ( ((plaintext[i] - 65) + (keyStream[i] - 65) + 1) % 26) + 65);
		}
		return result;
	}
	
	public static char[] decrypt(char[] plaintext, char[] keyStream)
	{
		char[] result = new char[keyStream.length];
		for(int i = 0; i < result.length; i++)
		{
			result[i] = (char)( ( ((plaintext[i] - 65) - (keyStream[i] - 65 + 1) + 26) % 26) + 97);
		}
		return result;
	}



}
