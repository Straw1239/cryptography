package specialized.cadenus;

import specialized.cadenus.Cadenus.Key;

public class TabuSearcher
{
	private Key state;
	private Key bestSoFar;
	private final String text;
	private final int keyLength;
	private int[][] transpositionLog;
	private int[][] shiftLog; 
	private double[][] transpositionScoreSums;
	private double[][] shiftScoreSums;
	
	public TabuSearcher(String text)
	{
		if(text.length() % 25 != 0) throw new IllegalArgumentException();
		this.text = text;
		keyLength = text.length() / 25;
		transpositionLog = new int[keyLength][keyLength];
		shiftLog = new int[keyLength][25];
		transpositionScoreSums= new double[keyLength][keyLength];
		shiftScoreSums = new double[keyLength][25];
		state = new Key(keyLength);
		bestSoFar = new Key(state);
		throw new UnsupportedOperationException();
	}
}
