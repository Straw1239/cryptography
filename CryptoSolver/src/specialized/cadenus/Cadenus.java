package specialized.cadenus;
import static utils.Utils.compute;
import static utils.Utils.rand;
import static utils.Utils.swap;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Predicate;

import main.FinalMain;
import utils.IntArraySwapper;
import utils.Search;
import utils.SettableSearch;
import utils.TabuCache;
import utils.Utils;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;

import english.English;


public class Cadenus
{
	private static final Predicate<Key> TRUE = k -> true;
	public static Key parseKey(String data)
	{
		Key result = new Key();
		Scanner s = new Scanner(data);
		s.useDelimiter("[, \\[\\]]+");
		s.next();
		s.next();
		int size = 0;
		int[] transpos = new int[10];
		while(s.hasNextInt())
		{
			if(size >= transpos.length)
			{
				transpos = Arrays.copyOf(transpos, transpos.length * 2);
			}
			transpos[size++] = s.nextInt();
			
		}
		transpos = Arrays.copyOf(transpos, size);
		int[] shifts = new int[size];
		int index = 0;
		s.next();
		s.next();
		while(s.hasNextInt())
		{
			shifts[index++] = s.nextInt();
			
		}
		s.close();
		if(index != size) throw new IllegalArgumentException();
		result.transpos = transpos;
		result.shifts = shifts;
		return result;
	}
	
	private static final class KeyMutator
	{
		private Key state;
		private boolean shifting = false;
		private IntArraySwapper transMutator;
		private int data1 = -1, data2 = -1;
		private boolean isDataShift;
		public KeyMutator(Key k)
		{
			state = k;
			transMutator = new IntArraySwapper(state.transpos);
			oldShift = state.shifts[shiftIndex];
		}
		
		int shiftIndex = 0;
		int shift = 0;
		int oldShift;
		
		public boolean advance()
		{
			if(transMutator.hasNext()) 
			{
				transMutator.advance();
				return true;
			}
			else 
			{
				if(!shifting)
				{
					shifting = true;
					transMutator.revert();	
				}
				if(shift == oldShift) shift++;
				state.shifts[shiftIndex] = shift;
				shift++;
				if(shift <= 25)
				{
					return true;
				}
				else
				{
					state.shifts[shiftIndex] = oldShift;
					shiftIndex++;
					shift = 0;
					boolean hasNext = shiftIndex < state.shifts.length;
					if(hasNext)
					{
						oldShift = state.shifts[shiftIndex];
						if(shift == oldShift) shift++;
						state.shifts[shiftIndex] = shift;
						shift++;
					}
					return hasNext;
				}
				
			}
		}
		
		public void save()
		{
			isDataShift = shifting;
			if(isDataShift)
			{
				data1 = shiftIndex;
				data2 = (shift + 24) % 25; // Go back one step
			} 
			else
			{
				data1 = transMutator.firstSwapIndex();
				data2 = transMutator.secondSwapIndex();
			}
		}
		
		public void load()
		{
			if(isDataShift)
			{
				state.shifts[data1] = data2;
			}
			else
			{
				swap(state.transpos, data1, data2);
			}
		}
		
		public int swap1()
		{
			if(isDataShift) throw new IllegalStateException();
			return data1;
		}
		
		public int swap2()
		{
			if(isDataShift) throw new IllegalStateException();
			return data2;
		}
		
		public int shiftIndex()
		{
			if(!isDataShift) throw new IllegalArgumentException();
			return data1;
		}
		
		public int shift()
		{
			if(!isDataShift) throw new IllegalArgumentException();
			return data2;
		}
		
		public boolean isSavedShift()
		{
			return isDataShift;
		}

		public int firstSwapIndex()
		{
			return transMutator.firstSwapIndex();
		}

		public int secondSwapIndex()
		{
			return transMutator.secondSwapIndex();
		}

		public boolean isTransposing()
		{
			return !shifting;
		}

		public int getColumn()
		{
			return shiftIndex;
		}
		
	}

	public static final class Key 
	{
		public Key(Key state)
		{
			transpos = state.transpos.clone();
			shifts = state.shifts.clone();
		}
		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.hashCode(shifts);
			result = prime * result + Arrays.hashCode(transpos);
			return result;
		}
		@Override
		public boolean equals(Object obj)
		{
			if (this == obj) return true;
			if (obj == null) return false;
			if (!(obj instanceof Key)) return false;
			Key other = (Key) obj;
			if (!Arrays.equals(shifts, other.shifts)) return false;
			if (!Arrays.equals(transpos, other.transpos)) return false;
			return true;
		}
		@Override
		public String toString()
		{
			return "transpos = " + Arrays.toString(transpos) + ", shifts = "
					+ Arrays.toString(shifts);
		}
		public Key()
		{
			
		}
		public Key(int length)
		{
			transpos = new int[length];
			shifts = new int[length];
			Arrays.setAll(transpos, i -> i);
		}
		public Key(String key)
		{
			this(key.toCharArray());
		}
		public Key(char[] key)
		{
			transpos = getTransposition(key);
			shifts = getShifts(key);
		}
		public int[] transpos;
		public int[] shifts;

	}

	public static String encrypt(String text, String key)
	{
		return null;
	}
	
	public static char[] decrypt(String text, Key key)
	{
		//if(text.length() % 25 != 0) throw new IllegalArgumentException();
		int numColumns = text.length() / 25;
		//if(key.transpos.length != numColumns) throw new IllegalArgumentException();
		char[] decrypted = new char[text.length()];
		for(int i = 0; i < 25; i++)
		{
			for(int j = 0; j < numColumns; j++)
			{
				int index = j + numColumns * i;
				int transpos = key.transpos[j];
				decrypted[transpos + ((25 - key.shifts[transpos] + i) % 25) * numColumns] = text.charAt(index);
			}
		}
		return decrypted;
	}

	
	public static char[] decrypt(String text, char[] key)
	{
		return decrypt(text, new Key(key));
	}
	
	
	
	private static int[] getShifts(char[] key)
	{
		int[] result = new int[key.length];
		for(int i = 0; i < key.length; i++)
		{
			result[i] = getIndex(key[i]);
		}
		return result;
	}

	private static int getIndex(char c)
	{
		if(c <= 'u') return Utils.lowercaseIndex(c);
		switch(c)
		{
		case 'v': return 21;
		case 'w': return 21;
		case 'x': return 22;
		case 'y': return 23;
		case 'z': return 24;
		}
		throw new IllegalArgumentException("" + c);
	}

	public static int[] getTransposition(char[] key)
	{
		ListMultimap<Character, Integer> m = Multimaps.newListMultimap(new TreeMap<>(), ArrayList::new);
		for(int i = 0; i < key.length; i++)
		{
			m.put(key[i], i);
		}
		int[] result = new int[key.length];
		int j = 0;
		for(int i : m.values())
		{
			result[j] = i;
			j++;
		}
		return result;
	}
	
	public static Search<Key> solve(String text)
	{
		if(text.length() % 25 != 0) throw new IllegalArgumentException();
		String l = text.toLowerCase();
		SettableSearch<Key> result = new SettableSearch<>();
		compute.submit(() -> 
		{
			try
			{
				solve(l, result);
			}
			catch(Throwable t)
			{
				t.printStackTrace();
			}
		});
		return result;
	}
	
	public static Search<Key> tsolve(String text)
	{
		if(text.length() % 25 != 0) throw new IllegalArgumentException();
		String l = text.toLowerCase();
		SettableSearch<Key> result = new SettableSearch<>();
		compute.submit(() -> 
		{
			try
			{
				tabuSolve(l, result);
			}
			catch(Throwable t)
			{
				t.printStackTrace();
			}
		});
		return result;
	}
	public static TreeMultimap<Double, Key> bestStates = TreeMultimap.create(Ordering.natural(), Ordering.arbitrary());
	
	public static long defaultMaxIterations = 5_000_000_000L;
	
	private static void solve(String text, SettableSearch<Key> out)
	{
		final int length = text.length() / 25;
		Key state = new Key(length);
		Arrays.setAll(state.transpos, i -> i);
		Arrays.fill(state.shifts, 0);
		randomize(state);
		Key best = new Key(state);
		double currentScore = score(text, state);
		double bestScore = currentScore;
		long iterations = 0;
		long iterationsSinceImprovement = 0;
		int kicks = 0;
		final long maxIterations = defaultMaxIterations;
		final long defaultKick = (maxIterations * 11) / 10;
		long kickThreshold = defaultKick;
		while(!out.shouldStop())
		{
			if(iterationsSinceImprovement > kickThreshold)
			{
				iterations = maxIterations - (1L << (kicks + 16));
				kicks++;
				kickThreshold += maxIterations - iterations + maxIterations / 10;
			}
			double temp = (1 - iterations * 1.0 / maxIterations) / 10;
			if(rand.nextBoolean())
			{
				//Mutate columns
				int swap1 = rand.nextInt(length);
				int swap2 = rand.nextInt(length);
				while(swap1 == swap2) swap2 = rand.nextInt(length);
				swap(state.transpos, swap1, swap2);
				double score = score(text, state);
				if(score > bestScore)
				{
					bestScore = score;
					currentScore = score;
					best = new Key(state);
					out.setResult(best);
					bestStates.put(bestScore, best);
					if(bestStates.size() > 10) bestStates.asMap().remove(bestStates.asMap().firstKey());
					//System.err.println("new best:" + score);
					iterationsSinceImprovement = 0;
					kicks = 0;
					kickThreshold = defaultKick;
				}
				else 
				{	
					if (accept(currentScore, score, temp))
					{
						currentScore = score;
					}
					else swap(state.transpos, swap1, swap2);
					iterationsSinceImprovement++;
				}
			} 
			else
			{
				//Mutate shifts
				int column = rand.nextInt(length);
				int oldShift = state.shifts[column];
				int newShift = rand.nextInt(25);
				state.shifts[column] = newShift;
				double score = score(text, state);
				if(score > bestScore)
				{
					bestScore = score;
					currentScore = score;
					best = new Key(state);
					out.setResult(best);
					bestStates.put(bestScore, best);
					if(bestStates.size() > 10) bestStates.asMap().remove(bestStates.asMap().firstKey());
					iterationsSinceImprovement = 0;
					kicks = 0;
					kickThreshold = defaultKick;
				}
				else 
				{
					if(accept(currentScore, score, temp))
					{
						currentScore = score;
					}
					else state.shifts[column] = oldShift;
					iterationsSinceImprovement++;
				}
			}
			//if(iterations % 100 == 0) System.err.println(currentScore);
			iterations++;
		}
	}
	
	

	private static void randomize(Key state)
	{
		Collections.shuffle(Arrays.asList(state.transpos));
		for(int i = 0; i < state.shifts.length; i++)
		{
			state.shifts[i] = rand.nextInt(25);
		}
	}
	static long t = 0;
	private static boolean accept(double currentScore, double newScore, double temperature)
	{
		//t++;
		if(newScore > currentScore) return true;
		double probability = Math.exp(-(currentScore - newScore) / temperature);
		//if(t % 10000 == 0) System.err.println(probability * 100 + "%");
		return rand.nextDouble() <= probability;
	}
	private static char[] bonus = "phaseeight".toCharArray(); 
	static double score(String ciphertext, Key state)
	{
		char[] result = decrypt(ciphertext, state);
		double score = English.logScore(result);
		//double multi = 1.1;
		//for(int i = 0; i < bonus.length; i++)
		{
			//if(result[i] == bonus[i]) score *= multi;
		}
		//score += English.quadgraphScore(key) * .3;
		return score;
	}
	
	private static void tabuSolve(String text, SettableSearch<Key> out)
	{
		final int keyLength = text.length() / 25;
		int[][] transpositionLog = new int[keyLength][keyLength];
		int[][] shiftLog = new int[keyLength][25];
		double[][] transpositionScoreSums = new double[keyLength][keyLength];
		double[][] shiftScoreSums = new double[keyLength][25];
		Key state = out.bestSoFar();
		if(state == null) state = new Key(keyLength);
		//randomize(state);
		double score = score(text, state);
		double bestScore = score;
		Key bestState = new Key(state);
		final int tabuSize = 12;
		final int shiftTabuSize = 15;
		final int shortTabuSize = 2;
		final int shortShiftTabuSize = 4;
		Deque<Predicate<Key>> transpositionTabu = new ArrayDeque<>(tabuSize);
		TabuCache<Integer> shortTransTabu = new TabuCache<>(shortTabuSize * 2);
		TabuCache<Integer> shortShiftTabu = new TabuCache<>(shortShiftTabuSize);
		Deque<Predicate<Key>> shiftTabu = new ArrayDeque<>(shiftTabuSize);
		long iterationsSinceChange = 0;
		long iterations = 0;
		while(!out.shouldStop())
		{
			if(iterationsSinceChange > 5000)
			{
				iterationsSinceChange = -5000;
				state = diversify(transpositionLog, shiftLog);
			}
			iterationsSinceChange++;
			double bestNeighborScore = Double.NEGATIVE_INFINITY;
			double bestAltScore = Double.NEGATIVE_INFINITY;
			KeyMutator gen = new KeyMutator(state);
			while(gen.advance())
			{
				double newScore = score(text, state);
				if(newScore > bestScore)
				{
					gen.save();
					bestScore = newScore;
					bestState = new Key(state);
					bestNeighborScore = newScore;
					out.setResult(bestState);
					System.out.println("new best:" + bestScore);
					iterationsSinceChange = 0;
				}
				else if(gen.isTransposing() ? shortTransTabu.allow(gen.firstSwapIndex()) && shortTransTabu.allow(gen.secondSwapIndex()) && allow(state, transpositionTabu)  
						: shortShiftTabu.allow(gen.getColumn()) && allow(state, shiftTabu))
				{
					if(newScore > bestNeighborScore)
					{
						bestNeighborScore = newScore;
						gen.save();
					}
					if(bestNeighborScore <= score)
					{
						double altScore = nonImprovingScore(text, state);
						if(altScore > bestAltScore)
						{
							bestAltScore = altScore;
						}
					}
				}
			}
			//if(bestNeighborScore <= score) // If no moves are directly improving, use alternate heuristic to judge
			{
				//swap1 = altSwap1;
				//swap2 = altSwap2;
				//column = altColumn;
				//shift = altShift;
			}
			if(!gen.isSavedShift())
			{
				final int fswap1 = gen.swap1();
				final int fswap2 = gen.swap2();
				final int atswap1 = state.transpos[fswap1];
				final int atswap2 = state.transpos[fswap2];
				transpositionTabu.add(k -> 
				{
					return k.transpos[fswap1] != atswap1 && k.transpos[fswap2] != atswap2;
				});
				shiftTabu.add(TRUE);
				shortTransTabu.add(gen.swap1());
				shortTransTabu.add(gen.swap2());
				shortShiftTabu.add(-1);
			}
			else
			{	
				final int fcolumn = gen.shiftIndex();
				final int oldShift = state.shifts[fcolumn];
				shiftTabu.add(k -> 
				{
					return k.shifts[fcolumn] != oldShift;
				});
				shortShiftTabu.add(fcolumn);
				transpositionTabu.add(TRUE);
				shortTransTabu.add(-1);
				shortTransTabu.add(-1);
			}
			gen.load();
			score = bestNeighborScore;
			if(iterations % 1000 == 0)
			{
				System.err.println(iterationsSinceChange);
				System.err.println(state);
				System.err.println(bestNeighborScore);
				for(int i = 0; i < transpositionScoreSums.length; i++)
				{
					for(int j = 0; j < transpositionScoreSums[i].length; j++)
					{
						System.err.print(transpositionScoreSums[i][j] / transpositionLog[i][j] + " ");
					}
				}
				System.err.println();
				for(int i = 0; i < shiftScoreSums.length; i++)
				{
					for(int j = 0; j < shiftScoreSums[i].length; j++)
					{
						System.err.print(shiftScoreSums[i][j] / shiftLog[i][j] + " ");
					}
				}
				System.err.println();
			}
			
			if(transpositionTabu.size() > tabuSize)
			{
				transpositionTabu.remove();
			}
			if(shiftTabu.size() > shiftTabuSize)
			{
				shiftTabu.remove();
			}
			for(int i = 0; i < keyLength; i++)
			{
				transpositionLog[i][state.transpos[i]]++;
				shiftLog[i][state.shifts[i]]++;
				transpositionScoreSums[i][state.transpos[i]] += score;
				shiftScoreSums[i][state.shifts[i]] += score;
			}
			iterations++;
			bestStates.put(bestNeighborScore, new Key(state));
			if(bestStates.size() > 10) bestStates.keySet().remove(bestStates.keySet().first());
		}
	}
	
	
	private static boolean allow(Key state, Iterable<Predicate<Key>> tabu)
	{
		for(Predicate<Key> p : tabu)
		{
			if(!p.test(state)) return false;
		}
		return true;
	}
	
	private static Key diversify(int[][] transposStats, int[][] shiftStats)
	{
		int[] transpos = new int[transposStats.length];
		Set<Integer> temp = new HashSet<>();
		for(int i = 0; i < transpos.length; i++)
		{
			transpos[i] = minIndex(transposStats[i], temp);
			temp.add(transpos[i]);
		}
		Key result = new Key();
		result.transpos = transpos;
		int[] shifts = new int[shiftStats.length];
		for(int i = 0; i < shifts.length; i++)
		{
			shifts[i] = minIndex(shiftStats[i]);
		}
		result.shifts = shifts;
		return result;
	}

	private static int minIndex(int[] data)
	{
		return minIndex(data, Collections.emptySet());
	}
	
	private static int minIndex(int[] data, Set<Integer> disallowed)
	{
		int minSoFar = Integer.MAX_VALUE;
		int minIndex = -1;
		for(int i = 0; i < data.length; i++)
		{
			if(disallowed.contains(i)) continue;
			minSoFar = Math.min(minSoFar, data[i]);
			if(minSoFar == data[i]) minIndex = i;
		}
		return minIndex;
	}

	public static Multimap<int[], String> findMatchingColumns(String text, int numToReport)
	{
		if(text.length() % 25 != 0) throw new IllegalArgumentException();
		int columns = text.length() / 25;
		Multimap<int[], String> result = HashMultimap.create();
		for(int i = 0; i < columns; i++)
		{
			for(int j = 0; j < columns; j++)
			{
				if(j == i) continue;
				for(int k = 0; k < columns; k++)
				{
					if(k == j || k == i) continue;
					for(int shift1 = 0; shift1 < 25; shift1++)
					{
						for(int shift2 = 0; shift2 < 25; shift2++)
						{
							
						}
					}
				}
			}
		}
		
		return result;
	}
	
	public static Set<Key> link(String text, Key a, Key b, double threshold)
	{
		Key state = new Key(a);
		int distance = countDifferences(state, b);
		Set<Key> results = new HashSet<>();
		while(distance != 0)
		{
			double bestNeighborScore = Double.NEGATIVE_INFINITY;
			KeyMutator gen = new KeyMutator(state);
			int distanceOfBest = distance;
			while(gen.advance())
			{
				double score = score(text, state);
				if(score > threshold) results.add(new Key(state));
				int newDistance = countDifferences(state, b);
				if(newDistance < distance)
				{
					if(score > bestNeighborScore)
					{
						bestNeighborScore = score;
						distanceOfBest = newDistance;
						gen.save();
					}
				}
			}
			gen.load();
			distance = distanceOfBest;
		}
		results.remove(b);
		results.remove(a);
		return results;
	}
	
	private static int countDifferences(Key state, Key b)
	{
		assert state.shifts.length == b.shifts.length;
		int count = 0;
		for(int i = 0; i < state.shifts.length; i++)
		{
			if(state.shifts[i] != b.shifts[i]) count++;
			if(state.transpos[i] != b.transpos[i]) count++;
		}
		return count;
	}

	public static double nonImprovingScore(String text, Key state)
	{
		return 0;
	}

	/*
	public static void main(String[] args)
	{
		String text = "aoempsnapeohetlhtvijisamaddumaedsrghtlaoihasesurtn";
		//String text = "anwstmndehiupyitrewaetheeIeduufieFihdwtveftdesleecPsiydbSnriehgtdrksvoiegchhmhsAnrtthattcditeiuidshelieeeietlwtonlstibsmeieibsdefdouFeopodlsneaihxdntitertaaentescrihaeeoiardSheasoleomatnedIwibaknniekiemaanacydtseaadentcestnielrllaetrptfaorszelhceaIrlrulmileoedtFdymhklgdvpiwuriatuhwdgeeioeiltrntcsjla";
		text = text.toLowerCase();
		//final String ptext = text;
		//Search<Key> s = tsolve(text);
		//s.addUpdateListener(System.out::println);
		//s.addUpdateListener(k ->
		//{
			//System.out.println(new String(decrypt(ptext, k)));
		//});
		//while(true)
		{
			//try
			{
			//	Thread.sleep(1000);
			}
			//catch (InterruptedException e)
			{
				//e.printStackTrace();
			}
			//System.out.print("ONE");
		}
		text = Utils.removePunctuation(text);
		//System.out.println(score(text, new Key("rg")));
		//System.out.println(new Key("rg"));
		System.out.println(decrypt(text, "rg".toCharArray()));
	}
	*/
	
	/*public static void main(String[] args) throws Exception
	{
		String text = FinalMain.letterCipher;
		List<Key> keys = new ArrayList<>();
		Scanner s = new Scanner(new File("goodKeys.txt"));
		while(s.hasNextLine())
		{
			String line = s.nextLine();
			if(!line.isEmpty()) keys.add(parseKey(s.nextLine()));
		}
		double threshold = 11;
		Set<Key> results = new HashSet<>();
		for(Key k : keys)
		{
			for(Key j : keys)
			{
				if(k != j)
				{
					Set<Key> path = link(text, k, j, threshold);
					results.addAll(path);
				}
			}
		}
		for(Key k : results) System.out.println(k);
		System.out.println(results.size());
		s.close();
	}*/
	
	public static void main(String[] args) throws FileNotFoundException
	{
		String text = FinalMain.letterCipher;
		List<Key> keys = new ArrayList<>();
		Scanner s = new Scanner(new File("linkresults"));
		while(s.hasNextLine())
		{
			String line = s.nextLine();
			if(!line.isEmpty()) keys.add(parseKey(s.nextLine()));
		}
		System.out.println(keys.stream().mapToDouble(k -> score(text, k)).max().getAsDouble());
	}
	
}
