package specialized.cadenus;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import com.google.common.collect.Collections2;

public class Permutations
{

	public static void main(String[] args)
	{
		List<String> columns = loadData(new File("XColumns.txt"));
		Collection<List<String>> permutations = Collections2.permutations(columns);
		List<List<String>> mutable = permutations.stream().map(e -> new ArrayList<>(e)).collect(toList());
		for(List<String> c : mutable)
		{
			c.remove(c.size() - 1);
			c.remove(c.size() - 1);
			c.remove(c.size() - 1);
		}
		mutable = mutable.stream().distinct().collect(toList());
		for(List<String> c : mutable)
		{
			printAsColumns(c);
			System.out.println();
		}
	}
	
	public static void printAsColumns(List<String> columns)
	{
		
		int length = columns.get(0).length();
		for(int index = 0; index < length; index++)
		{
			for(String s : columns)
			{
				System.out.print(s.charAt(index));
			}
			System.out.println();
		}
	}

	private static List<String> loadData(File file)
	{
		try(Scanner s = new Scanner(file))
		{
			String[] firstLine = s.nextLine().split("\\s+");
			List<StringBuilder> results = Arrays.asList(firstLine).stream().map(StringBuilder::new).collect(toList());
			while(s.hasNextLine())
			{
				String[] line = s.nextLine().split("\\s+");
				for(int i = 0; i < line.length; i++)
				{
					results.get(i).append(line[i]);
				}
			}
			return results.stream().map(Object::toString).collect(toList());
		}
		catch (FileNotFoundException e)
		{
			throw new RuntimeException(e);
		}
	}

}
