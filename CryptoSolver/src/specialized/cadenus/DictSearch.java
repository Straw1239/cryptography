package specialized.cadenus;

import specialized.cadenus.Cadenus.Key;
import english.English;
import main.FinalMain;

public class DictSearch
{
	public static void main(String[] args)
	{
		String text = FinalMain.letterCipher;
		
		int keyLength = text.length() / 25;
		Key best = new Key(keyLength);
		double bestScore = Cadenus.score(text, best);
		String bestWord = "";
		char[] key = new char[keyLength];
		for(String w : English.dictionary)
		{
			if(keyLength % w.length() == 0)
			{
				int factor = keyLength / w.length();
				for(int i = 0; i < factor; i++)
				{
					w.getChars(0, w.length(), key, i * w.length());
				}
				double score = Cadenus.score(text, new Key(key));
				if(score > bestScore)
				{
					bestScore = score;
					best = new Key(key);
					bestWord = w;
				}
			}
		}
		System.out.println(bestScore);
		System.out.println(best);
		System.out.println(bestWord);
	}
}
