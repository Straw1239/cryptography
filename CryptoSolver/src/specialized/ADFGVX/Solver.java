package specialized.ADFGVX;

import static utils.Utils.compute;
import specialized.ADFGVX.ADFGVX.Key;
import utils.Search;
import utils.SettableSearch;

public class Solver
{
	public static Search<Key> solve(String text)
	{
		SettableSearch<Key> search = new SettableSearch<>();
		compute.submit(() -> solve(text, search));
		return search;
	}
	
	
	private static void solve(String text, SettableSearch<Key> search)
	{
		Key state = initalGuess(text);
		while(!search.shouldStop())
		{
			
		}
	}


	private static Key initalGuess(String text)
	{
		return new Key("a", "abcde");
	}
}
