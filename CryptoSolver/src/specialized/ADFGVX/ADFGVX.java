package specialized.ADFGVX;

import static utils.Utils.compute;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

import utils.Search;
import utils.SettableSearch;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;

import english.Digraph;
import english.English;

public class ADFGVX
{
	public static final String ADFGVX = "ADFGVX";
	public static final int GRID_SIZE = ADFGVX.length() * ADFGVX.length();
	public static final String alphabet = English.alphabet + "0123456789";
	
	public static int[] getTransposition(String key)
	{
		key = key.toLowerCase();
		ListMultimap<Character, Integer> m = Multimaps.newListMultimap(new TreeMap<>(), ArrayList::new);
		for(int i = 0; i < key.length(); i++)
		{
			m.put(key.charAt(i), i);
		}
		int[] result = new int[key.length()];
		int j = 0;
		for(int i : m.values())
		{
			result[j] = i;
			j++;
		}
		return result;
	}
	
	public static char[] getMatrix(String key)
	{
		if(key.length() > GRID_SIZE) throw new IllegalArgumentException();
		key = key.toLowerCase();
		char[] result = new char[GRID_SIZE];
		Set<Character> usedChars = new HashSet<>();
		int index = 0;
		for(int i = 0; i < key.length(); i++)
		{
			if(!usedChars.contains(key.charAt(i)))
			{
				result[index] = key.charAt(i);
				usedChars.add(key.charAt(i));
				index++;
			}
		}
		for(int i = 0; index < result.length && i < alphabet.length(); i++)
		{
			if(!usedChars.contains(alphabet.charAt(i)))
			{
				result[index] = alphabet.charAt(i);
				usedChars.add(alphabet.charAt(i));
				index++;
			}
		}
		return result;
	}
	public static final class Key
	{
		private BiMap<Character, Digraph> sub = HashBiMap.create(GRID_SIZE); 
		private int[] transposition;
		
		public Digraph getSub(char c)
		{
			return sub.get(c);
		}
		
		public char getUnsubbed(Digraph d)
		{
			return sub.inverse().get(d);
		}
		
		private Key(int[] columns)
		{
			transposition = columns;
		}
		
		public Key(String subKey, String transKey)
		{
			this(getMatrix(subKey), transKey);
		}
		
		public Key(char[] matrix, String transposKey)
		{
			this(matrix, getTransposition(transposKey));
		}
		
		public Key(char[] matrix, int[] columns)
		{
			this(columns.clone());
			if(matrix.length != GRID_SIZE) throw new IllegalArgumentException();
			for(int i = 0; i < ADFGVX.length(); i++) // Rows
			{
				for(int j = 0; j < ADFGVX.length(); j++) // Columns
				{
					int index = ADFGVX.length() * i + j;
					char a = ADFGVX.charAt(i);
					char b = ADFGVX.charAt(j);
					sub.put(matrix[index], Digraph.of(a, b));
				}
			}
		}
	}
	
	
	public static char[] encrypt(String text, Key key)
	{
		char[] result = new char[text.length() * 2];
		for(int i = 0; i < text.length(); i++)
		{
			Digraph d = key.getSub(text.charAt(i));
			if(d == null) throw new IllegalArgumentException("Invalid Character in String: " + text.charAt(i));
			result[2 * i] = d.first;
			result[2 * i + 1] = d.second;
		}
		result = transpose(result, key);
		return result;
	}
	
	private static char[] transpose(char[] input, Key key)
	{
		char[] result = new char[input.length];
		int index = 0;
		int skip = key.transposition.length;
		for(int i : key.transposition)
		{
			for(int j = i; j < input.length; j += skip)
			{
				result[index] = input[j];
				index++;
			}
		}
		return result;
	}

	public static char[] decrypt(String text, Key key)
	{
		if(text.length() % 2 != 0) throw new IllegalArgumentException("Message length must be even");
		char[] result = unTranspose(text, key);
		char[] unSubbed = new char[text.length() / 2];
		for(int i = 0; i < unSubbed.length; i++)
		{
			unSubbed[i] = key.getUnsubbed(Digraph.of(result[i * 2], result[i * 2 + 1]));
		}
		return unSubbed;
	}
	
	private static char[] unTranspose(String text, Key key)
	{
		char[] result = new char[text.length()];
		int index = 0;
		int skip = key.transposition.length;
		for(int i : key.transposition)
		{
			for(int j = i; j < result.length; j += skip)
			{
				result[j] = text.charAt(index);
				index++;
			}
		}
		return result;
	}

	static double score(Key k, String ciphertext)
	{
		char[] decrypted = decrypt(ciphertext, k);
		return English.quadgraphScore(decrypted);
	}

	
	
	
}
