package gui;

import static general.Interface.*;
import static utils.Utils.*;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Consumer;














import specialized.cadenus.Cadenus;
import specialized.cadenus.Cadenus.Key;
import utils.Utils;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;














import com.google.common.util.concurrent.ListenableFuture;














import english.Decryption;

/**
 * User Interface code, all in static functions.
 * No one else needs to touch or understand any of this.
 * @author Rajan
 *
 */
public final class UI
{
	private UI(){};
	
	static Stage window;
	static TextArea inputText =  new TextArea();
	static TextArea plaintext = new TextArea();
	static TextArea output = new TextArea();
	static TextField console = new TextField("Enter Command...");
	static PrintStream out = new PrintStream(new OutputStream()
	{
		@Override
		public void write(int b) throws IOException
		{
			if(Platform.isFxApplicationThread()) output.appendText(String.valueOf((char) b));
			else Platform.runLater(() -> output.appendText(String.valueOf((char) b)));
		}
	});
	
	public static void launch(Stage s)
	{
		inputText.setEditable(false);
		inputText.setWrapText(true);
		plaintext.setEditable(false);
		plaintext.setWrapText(true);
		System.setOut(out);
		window = s;
		output.setEditable(false);
		GridPane pane = new GridPane();
		pane.add(inputText, 2, 0);
		pane.add(plaintext, 3, 0);
		pane.add(console, 2, 2);
		pane.add(output, 2, 1);
		Button solve = new Button("Solve");
		solve.setOnAction((e) -> 
		{
			ListenableFuture<Decryption> d = compute.submit(() -> aSolve(inputText.getText()));
			d.addListener(() -> 
			{
				try
				{
					setPlaintext(d.get().plaintext);
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}, exec);
		});
		pane.add(solve, 4, 0);
		console.addEventFilter(KeyEvent.KEY_PRESSED, (e) ->
		{
			if(e.getCode() == KeyCode.ENTER)
			{
				processCommand(console.getText());
				console.clear();
			}
		});
		Scene main = new Scene(pane);
		window.setHeight(600);
		window.setWidth(800);
		window.setScene(main);
		window.show();
	}
	
	
	private static Map<String, Consumer<String>> registeredCommands = new HashMap<>(); 
	
	public static void registerCommand(String command, Consumer<String> handler)
	{
		registeredCommands.put(command, handler);
	}
	
	private static void processCommand(String command)
	{
		Scanner s = new Scanner(command);
		if(!s.hasNext()) 
		{
			s.close();
			return;
		}
		Consumer<String> handler = registeredCommands.get(s.next());
		if(handler == null)
		{
			System.out.println("Command not recognized");
			s.close();
			return;
		}
		//exec.submit(() -> handler.accept(s.hasNextLine() ? s.nextLine() : ""));
		handler.accept(s.hasNextLine() ? s.nextLine().trim() : "");
		s.close();
	}
	
	public static void setMainText(String text)
	{
		inputText.setText(text);
	}
	
	public static void getCipherInput(Consumer<? super String> out)
	{
		Stage popup = new Stage();
		popup.initOwner(window);
		popup.setOnCloseRequest((e) -> 
		{
			//out.accept(null);
		});
		GridPane g = new GridPane();
		
		g.add(new Text("Enter Ciphertext here:"), 0, 0);
		TextArea text = new TextArea();
		g.add(text, 0, 1);
		Button b = new Button("Done");
		b.setOnAction((e) -> 
		{
			
			out.accept(Utils.removePunctuation(text.getText().toUpperCase()));
			popup.close();
		});
		g.add(b, 0, 2);
		popup.setScene(new Scene(g));
		popup.show();	
	}

	public static String inputText()
	{
		return inputText.getText();
	}
	
	public static void setPlaintext(String text)
	{
		plaintext.setText(text);
	}

	public static String plaintext()
	{
		return plaintext.getText();
	}
	
	private static final class GridLayout
	{
		private final String text;
		private final int width;
		private Key key;
		
		public GridLayout(String text, int width)
		{
			this.text = text;
			this.width = width;
			key = new Key(text.length() / 25);
			decrypted = new String(Cadenus.decrypt(text, key));
		}
		private String decrypted;
		public String decrypted()
		{
			return decrypted;
		}
		
		public void update()
		{
			decrypted = new String(Cadenus.decrypt(text, key));
		}
		
	}
	
	public static void createGrid(int columns)
	{
		Stage popup = new Stage();
		popup.initOwner(window);
		GridPane g = new GridPane();
		Text[][] charGrid = new Text[columns][25];
		Scene main =  new Scene(g);
		GridLayout grid = new GridLayout(inputText(), columns);
		
		for(int i = 0; i < charGrid.length; i++)
		{
			for(int j = 0; j < charGrid[i].length; j++)
			{
				int index = j * columns + i;
				charGrid[i][j] = new Text(grid.decrypted().substring(index, index + 1));
				final int fi = i;
				final int fj = j;
				
				charGrid[i][j].setOnMouseDragged(e -> 
				{
					Node source = (Node) e.getSource();
					GridPane.setColumnIndex(charGrid[fi][fj], GridPane.getColumnIndex(source));
					GridPane.setColumnIndex(source, fi);
					System.err.println("Drag");
				});
				
				charGrid[i][j].setFont(Font.font(java.awt.Font.MONOSPACED, 30));
				g.add(charGrid[i][j], i, j);
			}
		}
		g.setGridLinesVisible(true);
		
		
		
		popup.setScene(main);
		popup.show();
		
		
	}
}
