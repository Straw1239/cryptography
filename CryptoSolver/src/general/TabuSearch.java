package general;

import java.util.Collection;
import java.util.Collections;
import java.util.function.ToDoubleFunction;

public interface TabuSearch<T> extends RestartableSearch<T>
{
	public void setResolvingScorer(ToDoubleFunction<T> scorer);
	
	public default Collection<T> eliteSet()
	{
		return Collections.singleton(bestSoFar());
	}
	
	
	
	
}
