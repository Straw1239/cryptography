package general;

import java.util.List;

import english.Decryption;

public class Interface
{
	public static List<Decryption> generalSolutions(CharSequence text)
	{
		//Perform full general substitution, transposition, and hybrid decryption checks. Return all probable decryptions.
		return null;
	}
	
	public static Decryption gSolve(CharSequence text)
	{
		//Perform full general substitution, transposition, and hybrid decryption checks. Return the most probable decryption.
		return gSolve(text, 5);//What does depth do? determine magic number later.
	}
	
	public static Decryption gSolve(CharSequence text, int searchDepth)
	{
		return null;
	}
	
	public static Decryption subSolve(CharSequence text)
	{
		return null;
	}
	
	public static Decryption tSolve(CharSequence text)
	{
		return null;
	}
	
	public static Iterable<CharSequence> commonTranspositions(CharSequence text, int searchDepth)
	{
		return null;
	}
	
	public static List<Decryption> allSolve(CharSequence text)
	{
		return null;
	}
	
	public static Decryption aSolve(CharSequence text)
	{
		System.out.println("SOLVED!");
		return null;
	}
	
	
	
	
}
