package general;

import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.DoubleFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.function.UnaryOperator;

import utils.Utils;

public class General
{
	private static final double DEFAULT_MAX_TEMP = .000005;
	private static final double DEFAULT_TEMP_FALL_RATE = .00000000001;
	
	
	
	public static <T> T hillClimb(UnaryOperator<T> neighborGenerator, Function<T, Double> scorer, T state)
	{
		double bestScore = scorer.apply(state);
		int numFails = 0;
		while(numFails < 5000)
		{
			T t = neighborGenerator.apply(state);
			double score = scorer.apply(t);
			if(score > bestScore) 
			{
				state = t; 
				bestScore = score;
				numFails = 0;
			}
			else numFails++;
		}
		return state;
	}
	
	public static <T> T hillClimb(UnaryOperator<T> neighborGenerator, Function<T, Double> scorer, T state, int maxFails)
	{
		double bestScore = scorer.apply(state);
		int numFails = 0;
		while(numFails < maxFails)
		{
			T t = neighborGenerator.apply(state);
			double score = scorer.apply(t);
			if(score > bestScore) 
			{
				state = t; 
				bestScore = score;
				numFails = 0;
			}
			else numFails++;
		}
		return state;
	}
	
	/*
	 * Wikipedia pseudocode:
	 * s = s0; e = E(s)                          // Initial state, energy.               
		sbest = s; ebest = e                      // Initial "best" solution.             
		k = 0                                     // Energy evaluation count.             
		while k < kmax and e > emax               // While time left & not good enough:   
		  T = temperature(k/kmax)                 // Temperature calculation.             
		  snew = neighbour(s)                     // Pick some neighbour.                 
		  enew = E(snew)                          // Compute its energy.                  
		  if P(e, enew, T) > random() then        // Should we move to it?                
		    s = snew; e = enew                    // Yes, change state.                   
		  if enew < ebest then                    // Is this a new best?                  
		    sbest = snew; ebest = enew            // Save 'new neighbour' to 'best found'.
		  k = k + 1                               // One more evaluation done.            
		return sbest                              // Return the best solution found.      
	 */
	public static <T> T annealSim(UnaryOperator<T> neighborGenerator, ToDoubleFunction<T> scorer, T state)
	{
		return annealSim(neighborGenerator, scorer, state, DEFAULT_MAX_TEMP, DEFAULT_TEMP_FALL_RATE);
	}
	
	public static <T> T annealSim(UnaryOperator<T> neighborGenerator, ToDoubleFunction<T> scorer, T state, double maxTemp, double tempFallRate)
	{
		//long lastImprovement = 0;
		double currentScore = scorer.applyAsDouble(state);
		T bestSoFar = state;
		double bestScore = currentScore;
		double temp = maxTemp;
		while(temp > 0)
		{
			//lastImprovement++;
			T newState = neighborGenerator.apply(state);
			double newScore = scorer.applyAsDouble(newState);
			if(newScore > bestScore)
			{
				bestSoFar = newState;
				bestScore = newScore;
				state = newState;
				currentScore = newScore;
				//lastImprovement = 0;
			}
			else if(accept(currentScore, newScore, temp))
			{
				state = newState;
				currentScore = newScore;
			}
			temp -= tempFallRate;
		}
		return bestSoFar;
	}
	
	private static boolean accept(double currentScore, double newScore, double temperature)
	{
		if(newScore > currentScore) return true;
		double probability = Math.exp(-(currentScore - newScore) / temperature);
		
		return Utils.rand.nextDouble() <= probability;
	}
	
	public static <T> TabuSearch<T> tabuSearch(T initialState, Function<T, Iterable<T>> neighborhood, ToDoubleFunction<T> scorer, Recorder<T> memory)
	{
		
		return null;
	}
}
