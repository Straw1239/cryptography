package general;

public interface Recorder<T>
{
	public void record(T value);
	
	public boolean isAllowed(T value);
}
