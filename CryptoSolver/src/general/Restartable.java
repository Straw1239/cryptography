package general;

public interface Restartable<T>
{
	public void restart(T arg);
}
