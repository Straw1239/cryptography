package general;

import static english.English.alphabet;
import static utils.Utils.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import statistics.MonoFrequencies;
import utils.CharArraySwapper;
import utils.IntPair;
import utils.Search;
import utils.SettableSearch;
import utils.TabuCache;
import utils.Utils;
import english.Decryption;
import english.English;

public final class Substitution
{
	//static final MonoFrequencies english = MonoFrequencies.ofEnglish();
	//static final double FREQUENCY_ANALYZE_THRESHOLD = 6.00;
	//static final double MAX_FREQUENCY_RATIO = 1.3;
	
	/**
	 * Analyzes and attempts to decrypt monoalphabetic substitution ciphers. 
	 * @param ciphertext to decrypt
	 * @return
	 */
	/*
	public static Decryption monoSubSolve(String text)
	{
		Decryption result = new Decryption();
		double[][] probs = new double[26][26];
		
		
		
		return result;
	}
	*/
	/*
	public static Decryption monoSubSolve(String text)
	{
		Decryption result = new Decryption();
		result.ciphertext = text;
		MonoFrequencies freq = MonoFrequencies.of(text);
		NavigableMap<Double, Set<Character>> expected = english.frequencies();
		Multimap<Character, Character> probableSubs = LinkedHashMultimap.create();
		for(Entry<Double, Character> en : freq.frequenciesDescending())
		{
			char f = en.getValue();
			double d = en.getKey() * 100;
			if(d < FREQUENCY_ANALYZE_THRESHOLD) break;
			double max = d * MAX_FREQUENCY_RATIO;
			double min = d * (2 - MAX_FREQUENCY_RATIO);
			for(Entry<Double, Set<Character>> e : Utils.rangeSearch(expected, max, min))
			{
				for(char c : e.getValue())
				{
					probableSubs.put(f, c);
				}
			}
		}
		Set<Character> toRemove = new HashSet<>();
		for(Collection<Character> c : probableSubs.asMap().values())
		{
			Iterator<Character> it = c.iterator();
			while(it.hasNext())
			{
				if(toRemove.contains(it.next()))
				it.remove();
			}
			if(c.size() == 1)
			{
				toRemove.add(c.iterator().next());
			} 
		}
		
		BiMap<Character, Character> sub = HashBiMap.create();
		ImmutableMultimap<Character, Character> pSubs = ImmutableMultimap.copyOf(probableSubs);
		for(Entry<Character, Collection<Character>> e : pSubs.asMap().entrySet())
		{
			for(Character c : e.getValue())
			{
				if(sub.containsValue(c)) continue;
				sub.put(e.getKey(), c);
				break;
			}
		}
		ImmutableList<Character> keys = pSubs.keySet().asList();
		int[] position = new int[keys.size()];
		for(int i = 0; i < position.length; i++)
		{
			position[i] = pSubs.get(keys.get(i)).asList().indexOf(sub.get(keys.get(i)));
		}
		combinations: while(true) //We break out of this when all probable subs have been handled
		{
			int i;
			update: for(i = position.length - 1; i >= 0; i--) // For each key, in order of least frequent to most frequent
			{
				int maxSize = pSubs.get(keys.get(i)).size(); // Number of probable mappings for this key
				sub.remove(keys.get(i));//Remove the current mapping from the substitution, as we are changing it
				position[i]++; // Move to the next mapping for the current key
				if(position[i] >= maxSize)//If there there are no more mappings for this key
				{
					position[i] = 0; //Go to the first mapping
					continue; //Update next key
				}
				else while(sub.containsValue(pSubs.get(keys.get(i)).asList().get(position[i]))) //while this mapping is already contained in earlier keys, is illegal
				{
					position[i]++; //try the next mapping
					if(position[i] >= maxSize) //If there there are no more mappings for this key
					{
						position[i] = 0; //Go to the first mapping
						continue update; //Update next key
					}
				}
				break; // If we get here, no more key's mappings need to be changed in position[] 
			}
			if(i == -1) i += 1;	
			//New mappings need to be put into the substitution map
			sub.put(keys.get(i), pSubs.get(keys.get(i)).asList().get(position[i])); //add the current mapping from position to sub
			int temp = i;
			for(i = i + 1; i < position.length; i++) // Add all other mappings we deleted back to sub, constrained with our current mappings
			{
				while(sub.containsValue(pSubs.get(keys.get(i)).asList().get(position[i])))
				{
					position[i]++;
				}
				sub.put(keys.get(i), pSubs.get(keys.get(i)).asList().get(position[i]));
			}
			//Sub map is complete here, for each probable sub, do whatever computation here
			
			
			if(temp == 0) break combinations;
		}
		
		
		
		
		
		
		
		
		return result;
	}
	*/
	
	public static Decryption monoSubSolve(String text)
	{
		Decryption result = new Decryption();
		result.ciphertext = text;
		
		char[] sub = alphabet.toCharArray();
		
		char[] maximized = General.annealSim((m) -> 
		{
			char[] x = Arrays.copyOf(m, m.length);
			int swap = Utils.rand.nextInt(alphabet.length());
			int swap2 = Utils.rand.nextInt(alphabet.length());
			while(swap2 == swap)
			{
				swap2 = Utils.rand.nextInt(alphabet.length());
			}
			char temp = x[swap];
			x[swap] = x[swap2];
			x[swap2] = temp;
			return x;
		} , (m) -> score(m, text), sub);
		
		System.out.println(maximized);
		result.plaintext = applySub(maximized, text).toString().toLowerCase();
		return result;
		
	}
	
	private static String applySub(Map<Character, Character> sub, String text)
	{
		char[] decrypted = new char[text.length()];
		for(int i = 0; i < text.length(); i++)
		{
			decrypted[i] = sub.get(text.charAt(i));
		}
		return new String(decrypted);
	}

	private static <K, V> void randomize(Map<K, V> sub)
	{
		List<K> keys = new ArrayList<>(sub.keySet());
		List<V> values = new ArrayList<>(sub.values());
		Collections.shuffle(values);
		for(int i = 0; i < keys.size(); i++)
		{
			sub.replace(keys.get(i), values.get(i));
		}
	}
	
	private static double score(Map<Character, Character> m, String text)
	{
		char[] decrypted = new char[text.length()];
		for(int i = 0; i < text.length(); i++)
		{
			decrypted[i] = m.get(text.charAt(i));
		}
		return English.quadgraphScore(decrypted);
	}


	
	public static double score(char[] sub, String text)
	{
		char[] decrypted = new char[text.length()];
		for(int i = 0; i < text.length(); i++)
		{
			decrypted[i] = sub[Utils.uppercaseIndex(text.charAt(i))];
		}
		return English.logScore(decrypted);
	}
	
	public static String applySub(char[] sub, String text)
	{
		char[] decrypted = new char[text.length()];
		for(int i = 0; i < text.length(); i++)
		{
			decrypted[i] = sub[Utils.indexOf(text.charAt(i))];
		}
		return new String(decrypted);
	}

	
	public static Search<char[]> tabuSubSolve(String text)
	{
		SettableSearch<char[]> search = new SettableSearch<>(alphabet.toCharArray());
		compute.submit(() -> 
		{
			try
			{
				tabuSolve(search, text);
			}
			catch (Throwable t)
			{
				t.printStackTrace();
				System.exit(-1);
			}
			
		});
		return search;
	}
	
	
	
	private static void tabuSolve(SettableSearch<char[]> out, String text)
	{
		char[] state = likelyKey(text);
		//state = alphabet.toCharArray();
		//Collections.shuffle(Arrays.asList(state));
		char[] bestState = state.clone();
		double currentScore = score(state, text);
		double bestScore = currentScore;
		TabuCache<IntPair> tabu = new TabuCache<>(300);
		while(!out.shouldStop())
		{
			CharArraySwapper gen = new CharArraySwapper(state);
			double bestNext = Double.NEGATIVE_INFINITY;
			char[] bestNextState = state;
			int index1 = -1, index2 = -1;
			while(gen.hasNext())
			{
				gen.advance();
				double score = score(state, text);
				if(score > bestScore)
				{
					bestScore = score;
					bestState = state.clone();
					currentScore = score;
					bestNext = score;
					bestNextState = bestState;
					index1 = gen.firstSwapIndex();
					index2 = gen.secondSwapIndex();
					out.setResult(bestState);
					continue;
				}
				else
				{
					if(tabu.get(IntPair.of(gen.firstSwapIndex(), gen.secondSwapIndex())))
					{
						//TABU!
						continue;
					}
					else
					{
						if(score > bestNext)
						{
							bestNext = score;
							bestNextState = state.clone();
							index1 = gen.firstSwapIndex();
							index2 = gen.secondSwapIndex();
						}
					}
				}
			}
			tabu.put(IntPair.of(index1, index2), true);
			state = bestNextState;
		}
	}
	
	

	private static char[] likelyKey(String text)
	{
		MonoFrequencies m = MonoFrequencies.of(text);
		char[] state = new char[alphabet.length()];
		Arrays.fill(state, '?');
		Iterator<Entry<Double, Character>> en = MonoFrequencies.ofEnglish().frequenciesDescending().iterator();
		for(Entry<Double, Character> e : m.frequenciesDescending())
		{
			state[Utils.indexOf(e.getValue())] = Character.toLowerCase(en.next().getValue());
		}
		for(char c : alphabet.toCharArray())
		{
			String s = new String(state);
			if(s.indexOf(c) < 0)
			{
				int index = s.indexOf('?');
				state[index] = c;
			}
		}
		return state;
	}
	
	
	
}
