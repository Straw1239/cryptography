package general;

import utils.Search;

public interface RestartableSearch<T> extends Search<T>, Restartable<T>
{

}
