package main;

public class Test
{
	static String text = "mASKeDMAkEDmAskemASkedmAkEDMaSkemaSkedmakEDmaskEmaSkedmakEDmASkEmASKEdmAkEDMasKEmASKeDmakEDmaSkEmASKedMakEDMAskEmaSkedma";
	public static void main(String[] args)
	{
		int counter = 0;
		
		StringBuilder s = new StringBuilder();
		for(int i = 0; i < text.length(); i += 8)
		{
			int c = 0;
			for(int j = 0; j < 8; j++)
			{
				int bit = (Character.isUpperCase(text.charAt(i + j)) ? 1 : 0);
				c += bit << (7 - j);
			}
			s.append((char)c);
		}
		System.out.println(s);
	}

}
