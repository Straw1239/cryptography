package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import specialized.cadenus.Cadenus.Key;
import utils.Search;
import utils.Utils;
import english.English;

public class FinalMain
{
	static final String cipher = loadCipher();

	static final String alphabet = English.alphabet;
	public static final String letterCipher = getCipher();
	
	public static void main(String[] args)
	{
		System.out.println(letterCipher.substring(0, 175));
		split(letterCipher, 56).forEach((s) -> 
		{
			for(int i = 0; i < s.length(); i++)
			{
				//if(s.charAt(i) == 'q') System.err.println('q');
				//System.out.print(s.charAt(i));
			}
			//System.out.println();
		});
	}
	/*public static void main(String[] args) throws InterruptedException
	{
		List<Search<Key>> searches = new LinkedList<>();
		List<String> strings = new ArrayList<>();
		for(int i = 2; i <= 28; i++)
		{
			String subCipher = letterCipher.substring(0, i * 25);
			strings.add(subCipher);
			Search<Key> decryption = Cadenus.solve(subCipher);
			searches.add(decryption);
		}
		List<Key> results = new ArrayList<>(searches.size());
		Iterator<Search<Key>> itr = searches.iterator();
		while(!searches.isEmpty())
		{
			Thread.sleep(60 * 5 * 1000);
			for(int i = 0; i < 4; i++) 
			{
				if(itr.hasNext())
				{
					Search<Key> s = itr.next();
					results.add(s.bestSoFar());
					s.stop();
					itr.remove();
					System.out.println(s.bestSoFar());
				}
			}
		}
		for(int i = 0; i < results.size(); i++)
		{
			System.out.println(Cadenus.decrypt(strings.get(i), results.get(i)));
			System.out.println(Cadenus.score(strings.get(i), results.get(i)));
			System.out.println(results.get(i));
		}
		Utils.compute.shutdown();
		/*split(letterCipher, 56).forEach((s) -> 
		{
			for(int i = 0; i < s.length(); i++)
			{
				if(s.charAt(i) == 'q') System.err.println('q');
				//System.out.print(s.charAt(i) + " ");
			}
			System.out.println();
		});
		
	}*/
	
	private static String getCipher()
	{
		StringBuilder chars = new StringBuilder();
		split(cipher, 5).stream().mapToInt((s) -> Integer.parseInt(s, 2)).forEachOrdered(i -> chars.append(alphabet.charAt(i)));
		//chars.delete(0, 1);
		return chars.toString();
	}

	public static String reverse(String text)
	{
		StringBuilder b = new StringBuilder(text.length());
		for(int i = text.length() - 1; i >= 0; i--)
		{
			b.append(text.charAt(i));
		}
		return b.toString();
	}
	
	public static List<String> split(String text, int size)
	{
		List<String> result = new ArrayList<>(text.length() / size);
		for(int i = size; i < text.length(); i += size)
		{
			result.add(text.substring(i - size, i));
		}
		if(text.length() % size == 0)
		result.add(text.substring(text.length() - size, text.length()));
		return result;
	}
	
	public static char fromASCII(String fragment)
	{
		int num = Integer.parseInt(fragment, 2);
		return (char) num;
	}
	
	
	
	
	
	
	
	
	private static String loadCipher()
	{
		Scanner s;
		try
		{
			s = new Scanner(new File("final.txt"));
		}
		catch (FileNotFoundException e)
		{
			throw new RuntimeException(e);
		}
		StringBuilder builder = new StringBuilder();
		while(s.hasNextLine()) builder.append(s.nextLine());
		s.close();
		return builder.toString();
	}
}
