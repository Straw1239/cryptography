package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Countries
{
	private static final File LIST_LOCATION = new File("Countries.txt");
	static String[] countries = loadCountries(LIST_LOCATION);
	
	String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static boolean isLegal(char c)
	{
		switch(c)
		{
		case 'A':
		case 'B':
		case 'D':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		return false;
		default:
			return true;
		}
	}
	
	static boolean isValid(String arg)
	{
		for(int i = 0; i < arg.length(); i++)
		{
			if(!isLegal(arg.charAt(i))) return false;
		}
		return true;
	}
	public static void main(String[] args)
	{
		for(String s : countries)
		{
			if(isValid(s)) System.out.println(s);
		}
	}
	private static String[] loadCountries(File f)
	{
		List<String> temp = new ArrayList<>();
		try(Scanner s = new Scanner(f))
		{
			while(s.hasNextLine())
				temp.add(s.nextLine().toUpperCase());
		}
		catch (FileNotFoundException e)
		{
			throw new IllegalArgumentException(e);
		}
		return temp.toArray(new String[temp.size()]);
	}

}
