package main;

import static utils.Utils.compute;
import static utils.Utils.exec;
import english.English;
import general.Substitution;
import gui.UI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import specialized.cadenus.Cadenus;
import specialized.cadenus.Cadenus.Key;
import statistics.MonoFrequencies;
import utils.Search;


/**
 * Main class for application.
 * Launches GUI, performs all setup, initializes event listeners.
 * @author Rajan
 *
 */
public class Main extends Application
{
	public static final double VERSION = 0.1;

	public static void main(String[] args)
	{
		launch(args);
		System.exit(0);
	}

	public void init()
	{
		Platform.setImplicitExit(true);
	}
	
	public void stop()
	{
		compute.shutdown();
		exec.shutdown();
	}
	List<Search<?>> searches = new ArrayList<>();
	
	@Override
	public void start(Stage s) throws Exception
	{
		s.setTitle("CryptoSolver v" + VERSION);
		UI.launch(s);
		UI.getCipherInput(t -> 
		{
			UI.setMainText(t.toUpperCase());
		});
		UI.registerCommand("help", c -> 
		{
			System.out.println("No, you help me first!");
		});
		UI.registerCommand("stats", c ->
		{
			System.out.println(MonoFrequencies.of(UI.inputText()));
			System.out.println(English.logScore(UI.inputText().toLowerCase()));
		});
		UI.registerCommand("sub", c -> 
		{
			UI.setPlaintext(Substitution.monoSubSolve(UI.inputText()).plaintext);
		});
		UI.registerCommand("tabu", c ->
		{
			Search<char[]> search = Substitution.tabuSubSolve(UI.inputText());
			search.addUpdateListener(k -> 
			{
				System.out.println(Arrays.toString(k));
				String u = Substitution.applySub(k, UI.inputText());
				Platform.runLater(() -> 
				{
					UI.setPlaintext(u);
				});
			});
			searches.add(search);
			
		});
		UI.registerCommand("cad", c ->
		{
			if(!c.isEmpty())
			{
				Cadenus.defaultMaxIterations = Long.parseLong(c);
			}
			try
			{
				Search<Key> search = Cadenus.solve(UI.inputText());
				search.addUpdateListener(k -> 
				{
					System.out.println(k);
					String u = new String(Cadenus.decrypt(UI.inputText(), k));
					Platform.runLater(() -> 
					{
						UI.setPlaintext(u);
					});
				});
				searches.add(search);
			}
			catch (Throwable t)
			{
				t.printStackTrace();
			}
		});
		UI.registerCommand("tcad", c ->
		{
			try
			{
				Search<Key> search = Cadenus.tsolve(UI.inputText());
				search.addUpdateListener(k -> 
				{
					System.out.println(k);
					String u = new String(Cadenus.decrypt(UI.inputText(), k));
					Platform.runLater(() -> 
					{
						UI.setPlaintext(u);
					});
				});
				searches.add(search);
			}
			catch (Throwable t)
			{
				t.printStackTrace();
			}
		});
	
		UI.registerCommand("stop", c -> 
		{
			for(Search<?> e : searches)
			{
				e.stop();
			}
			searches.clear();
		});
		UI.registerCommand("test", c -> 
		{
			long time = System.nanoTime();
			for(int i = 0; i < 1000; i++)
			{
				English.wordFrequency(UI.inputText().toUpperCase());
			}
			System.out.println((System.nanoTime() - time) / 1000_000_000.0);
		});
		UI.registerCommand("new", c ->
		{
			UI.getCipherInput(t -> 
			{
				UI.setMainText(t.toUpperCase());
			});
		});
		UI.registerCommand("score", c -> 
		{
			System.out.println(English.logScore(UI.plaintext().toLowerCase()));
		});
		UI.registerCommand("printBest", c -> 
		{
			System.out.println(Cadenus.bestStates);
		});
		UI.registerCommand("load", c -> 
		{
			English.quadgraphScore("");
		});
		UI.registerCommand("manipulate", c -> 
		{
			if(UI.inputText().length() % 25 == 0) UI.createGrid(UI.inputText().length() / 25);
			else System.out.println("Error: Length not a multiple of 25");
		});
		UI.registerCommand("pass", c -> 
		{
			UI.setPlaintext(UI.inputText());
		});
		
			
		
	}
}
