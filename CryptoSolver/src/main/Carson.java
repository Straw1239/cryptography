package main;

import general.Substitution;

import java.util.Arrays;

import utils.Search;
import static english.English.alphabet;
public class Carson
{
	static String cipher = "04 10 09 17 01 02 17 01 02 06 08 24 08 10 11 09 16 08 10 22 01 02 17 01 10 04 10 16 01 02 17 15 06 19 02 17 15 04 23 19 24 01 02 17 19 04 10 09 17 01 02 17 08 10 11 09 06 19 01 02 17 05 04 04 17 08 08 06 20 02 01 11 10 08 19 17 08 10 16 01 02 17 02 16 10 08 23 16 05 19 14 05 08 04 08 06 14 17";
	
	public static void main(String[] args)
	{
		String letters = Arrays.stream(cipher.split(" "))
								.mapToInt(Integer::parseInt)
								.map(i -> i - 1)
								.mapToObj(alphabet::charAt)
								.collect(StringBuilder::new, (sb, c) -> sb.append(c), (b1, b2) -> b1.append(b2))
								.toString().toUpperCase();
		System.out.println(letters);
		Search<char[]> results = Substitution.tabuSubSolve(letters);
		results.addUpdateListener(c -> System.out.println(new String(c)));
		System.out.println(new String(results.bestSoFar()));
		
	}

}
