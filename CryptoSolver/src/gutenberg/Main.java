package gutenberg;

import static utils.Utils.THREADS;
import static utils.Utils.compute;
import static utils.Utils.exec;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.DoubleStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import net.openhft.koloboke.collect.map.LongDoubleCursor;
import net.openhft.koloboke.collect.map.LongIntCursor;
import net.openhft.koloboke.collect.map.hash.HashLongDoubleMap;
import net.openhft.koloboke.collect.map.hash.HashLongDoubleMaps;
import net.openhft.koloboke.collect.map.hash.HashLongIntMap;
import net.openhft.koloboke.collect.map.hash.HashLongIntMaps;
import utils.CharIterator;
import utils.Utils;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import english.LongQuads;
import english.Quadgraph;
public class Main
{
	
	public static final String location = "D:\\Utilities\\WGet\\www.gutenberg.lib.md.us\\";
	public static final String target = "G:\\Gutenberg\\";
	public static final File targetFile = new File(target);
	private static volatile long n = 1;
	private static final Pattern START = Pattern.compile("\\*\\*\\*\\s*start\\s*of\\s*(this|the)\\s*project.*\\*\\*\\*", Pattern.CASE_INSENSITIVE ^ Pattern.DOTALL);
	private static final Pattern END = Pattern.compile("\\*\\*\\*\\s*end.*\\*\\*\\*", Pattern.CASE_INSENSITIVE ^ Pattern.DOTALL);
	private static final File countData = new File("counts.dat");
	public static void main(String[] args) throws IOException, InterruptedException, ExecutionException 
	{
		/*
		//HashLongIntMaps.newMutableMap();
		//HashLongIntMaps.getDefaultFactory().newUpdatableMap();
		//countQuadgraphs();
		//System.out.println(countQuadgraphs(new File(target + "9998.txt")).get(Quadgraph.of("ther").toLong()));
		//parallelFor(targetFile, f -> 
		//{
			//simpRemoveBoilerplate(f);
			
		//});
		
		//unzipAll();
		//removeDuplicates(target);
		//HashLongDoubleMap m = HashLongDoubleMaps.newUpdatableMap();
		//m = calcFrequencies(countQuadgraphs(new File(target + "1D.txt")));
		List<HashLongIntMap> frequencies = parallelFor(targetFile, 
			f -> 
			{
				HashLongIntMap m = countQuadgraphs(f);
				return (m != null) ? new ArrayList<>(Collections.singletonList(countQuadgraphs(f))) : Collections.emptyList();
			},
			(a, b) -> 
			{
				a.addAll(b);
				return a;
			}, Function.identity());
			
		ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(countData));
		//o.writeObject(frequencies);
		o.flush();
		o.close();
		*/
	}
	
	

	public static HashLongDoubleMap calcFrequencies(HashLongIntMap data)
	{
		HashLongDoubleMap results = HashLongDoubleMaps.newUpdatableMap(data.size());
		long totalGraphs = 0;
		LongIntCursor c = data.cursor();
		while(c.moveNext())
		{
			totalGraphs += c.value();
		}
		c = data.cursor();
		while(c.moveNext())
		{
			results.put(c.key(), c.value() * 1.0 / totalGraphs);
		}
		return results;
	}

	public static HashLongDoubleMap standardDeviations(Collection<HashLongDoubleMap> data, HashLongDoubleMap mean)
	{
		HashLongDoubleMap results = HashLongDoubleMaps.newUpdatableMap(mean.size());
		for(LongDoubleCursor c = mean.cursor(); c.moveNext();)
		{
			long quad = c.key();
			double stdev = standardDeviation(data.stream().mapToDouble(m -> m.get(quad)), c.value(), data.size());
			results.put(quad, stdev);
		}
		return results;
	}
	
	public static double standardDeviation(DoubleStream values, double mean, int entries)
	{
		return Math.sqrt(values.map(d -> d - mean).map(d -> d * d).sum() / entries);
	}
	
	

	private static void countQuadgraphs() throws IOException, FileNotFoundException
	{
		
		HashLongIntMap c = parallelFor(targetFile, Main::countQuadgraphs, (a, b) -> 
		{
			for(LongIntCursor bc = b.cursor(); bc.moveNext();)
			{
				a.merge(bc.key(), bc.value(), Integer::sum);
			}
			return a;
		}, d -> d);
		System.out.println(c.get(0));
		List<Map.Entry<Quadgraph, Integer>> entries = new ArrayList<>();
		for(Entry<Long, Integer> e : c.entrySet())
		{
			entries.add(Utils.entryFrom(LongQuads.toQuadgraph(e.getKey()), e.getValue()));
		}
		entries.sort((a, b) -> -Integer.compare(a.getValue(), b.getValue()));
		//System.out.println(entries);
		File output = new File("gutgraphs.txt");
		output.delete();
		if(!output.exists()) output.createNewFile();
		PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream(output)));
		for(Entry<Quadgraph, Integer> e : entries)
		{
			out.print(e.getKey());
			out.print(" ");
			out.println(e.getValue());
		}
		out.close();
	}
	
	
	
	private static HashLongIntMap countQuadgraphs(File f)
	{
		try(Scanner s = new Scanner(f))
		{
			//s.useDelimiter("[\\s\n]+");
			Iterator<String> lowercase = Utils.stream(s).map(String::toLowerCase).iterator();
			CharIterator itr = Utils.flatten(lowercase);
			HashLongIntMap result = LongQuads.countAlphaGraphs(itr);
			synchronized(System.out)
			{
				System.out.printf("Counted %d quadgraphs in file %s\n", result.get(0), f.getName());
			}
			
			return result;
		}
		catch (FileNotFoundException e)
		{
			throw new IllegalArgumentException(e);
		}
		catch(NoSuchElementException e)
		{
			synchronized(System.err)
			{
				System.err.println(f + " is incomplete");
			}
			e.printStackTrace();
			f.delete();
			//System.exit(-1);
			return HashLongIntMaps.newUpdatableMap();
		}
		
		
	}
	public static void removeEndingBoilerplate(File doc)
	{
		if(!doc.exists()) throw new IllegalArgumentException();
		Scanner s = null;
		PrintStream p = null;
		try
		{
			File temp = File.createTempFile(doc.getName(), ".tmp");
			p = new PrintStream(new BufferedOutputStream(new FileOutputStream(temp)));
			s = new Scanner(new BufferedInputStream(new FileInputStream(doc)));
			while(s.hasNextLine())
			{
				String nextLine = s.nextLine();
				if(nextLine.toLowerCase().matches("End of Project Gutenberg's.+".toLowerCase().intern())) break;
				p.println(nextLine);
			}
			p.close();
			s.close();
			Files.move(temp.toPath(), doc.toPath(), StandardCopyOption.REPLACE_EXISTING);
			synchronized(System.out)
			{
				System.out.println("Completed " + doc.getName());
			}
		}
		catch(IOException e)
		{
			//Do something?
			throw new RuntimeException(e);
		}
		finally
		{
			if(s != null) s.close();
			if(p != null) p.close();
		}
	}
	final static int endLineSkips = 15;//365
	final static int startLineSkips = 0;//25
	private static void simpRemoveBoilerplate(File doc)
	{
		
		if(!doc.exists()) throw new IllegalArgumentException();
		Scanner s = null;
		PrintStream p = null;
		try
		{
			File temp = File.createTempFile("gtn", ".txt", targetFile);
			p = new PrintStream(new BufferedOutputStream(new FileOutputStream(temp)));
			s = new Scanner(new BufferedInputStream(new FileInputStream(doc)));
			
			ArrayList<String> lines = new ArrayList<String>(1024);
			while(s.hasNextLine())
			{
				lines.add(s.nextLine());
			}
			for(int i = startLineSkips; i < lines.size() - endLineSkips; i++)
			{
				p.println(lines.get(i));
			}
			p.close();
			s.close();
			Files.move(temp.toPath(), doc.toPath(), StandardCopyOption.REPLACE_EXISTING);
			synchronized(System.out)
			{
				System.out.println("Completed " + doc.getName());
			}
		}
		catch(IOException e)
		{
			throw new IllegalArgumentException(e);
		}
		finally
		{
			if(s != null) s.close();
			if(p != null) p.close();
		}
	}
	
	public static void removeBoilerplate(File doc)
	{
		if(!doc.exists()) throw new IllegalArgumentException();
		Scanner s = null;
		PrintStream p = null;
		try
		{
			File temp = File.createTempFile(doc.getName(), ".tmp");
			p = new PrintStream(new BufferedOutputStream(new FileOutputStream(temp)));
			s = new Scanner(new BufferedInputStream(new FileInputStream(doc)));
			String beginning = s.findWithinHorizon(START, 0);
			if(!s.hasNextLine()) 
			{
				synchronized(System.out)
				{
					System.out.println(doc + " Already Complete");
				}
				return;
			}
			while(s.hasNextLine())
			{
				if(s.findInLine(END) == null)
				{
					String line = s.nextLine();
					p.println(line);
				}
				else
				{
					break;
				}
			}
			p.close();
			s.close();
			Files.move(temp.toPath(), doc.toPath(), StandardCopyOption.REPLACE_EXISTING);
			synchronized(System.out)
			{
				System.out.println("Completed " + doc.getName());
			}
		}
		catch(IOException e)
		{
			//Do something?
			throw new RuntimeException(e);
		}
		finally
		{
			if(s != null) s.close();
			if(p != null) p.close();
		}
		
		
	
	}
	
	public static <I, F> F parallelFor(File directory, Function<File, I> action, BinaryOperator<I> combiner, Function<I, F> converter)
	{
		final File[] files = directory.listFiles();
		ListenableFuture<List<I>> completion;
		@SuppressWarnings("unchecked") // This never leaves the function, so this is safe
		ListenableFuture<I>[] temp = new ListenableFuture[THREADS];
		for(int i = 0; i < THREADS; i++)
		{
			final int start = (i * files.length) / THREADS;
			final int end = ((i + 1) * files.length) / THREADS;
			temp[i] = exec.submit(() -> 
			{
				I total = null;
				for(int j = start; j < end; j++)
				{
					I x = action.apply(files[j]);
					if(total == null)
					{
						total = x;
					}
					else
					{
						//parallelize this?
						total = combiner.apply(total, x);
					}
				}
				return total;
			});
		}
		completion = Futures.allAsList(temp);
		try
		{
			List<I> results = completion.get();
			if(results.size() == 0) return null;
			I combined = results.parallelStream().reduce(combiner).get();
			F finalResult = converter.apply(combined);
			return finalResult;
		}
		catch (InterruptedException | ExecutionException e)
		{
			throw new IllegalArgumentException(e);
		}
	}
	
	public static void parallelFor(File directory, Consumer<File> action)
	{
		final File[] files = directory.listFiles();
		ListenableFuture<?> completion;
		ListenableFuture<?>[] temp = new ListenableFuture<?>[THREADS];
		for(int i = 0; i < THREADS; i++)
		{
			final int start = (i * files.length) / THREADS;
			final int end = ((i + 1) * files.length) / THREADS;
			temp[i] = exec.submit(() -> 
			{
				for(int j = start; j < end; j++)
				{
					action.accept(files[j]);
				}
			});
		}
		completion = Futures.allAsList(temp);
		try
		{
			completion.get();
		}
		catch (InterruptedException | ExecutionException e)
		{
			throw new IllegalArgumentException(e);
		}
	}
	
	
	public static void unzipAll()  throws InterruptedException, ExecutionException 
	{
		long time = System.nanoTime();
		List<Future<?>> results = new ArrayList<>();
		for(int i = 1; i <= 9; i++)
		{
			File file = new File(location + i);
			byte[] buffer = new byte[1 << 20];
			//forEachFileIn(file, (f) -> unzipFiltered(f, target, buffer));
			results.add(compute.submit(() -> forEachFileIn(file, (f) -> unzipFiltered(f, target, buffer))));
			System.out.println(i);
		}
		for(Future<?> f : results) f.get(); 
		System.out.println((System.nanoTime() - time) / 1_000_000_000.0);
	}
	
	public static void forEachFileIn(File directory, Consumer<File> consumer)
	{
		for(File f : directory.listFiles())
		{
			if(f.isDirectory()) forEachFileIn(f, consumer);
			else consumer.accept(f);	
		}
	
	}
	
	static Pattern bookNum = Pattern.compile("\\[e(book|text)\\s*#\\d+\\]", Pattern.CASE_INSENSITIVE);
	public static void orderNames(String directory)
	{
		n = 1;
		forEachFileIn(new File(directory), (f) ->
		{
			try
			{
				Files.move(f.toPath(), f.toPath().resolveSibling(n++ + "D.txt"));
			}
			catch (Exception e)
			{
				throw new RuntimeException(e);
			}
		});
	}
	
	
	public static void removeDuplicates(String directory)
	{
		Set<String> ids = new HashSet<>();
		Set<String> others = new HashSet<>();
		n = 1;
		forEachFileIn(new File(directory), (f) ->
		{
			try
			{
				Scanner s = new Scanner(f);
				String id = s.findWithinHorizon(bookNum, Integer.MAX_VALUE);
				if(id == null)
				{
					String data = s.findWithinHorizon(Pattern.compile(".*", Pattern.DOTALL), 1000);
					if(others.contains(data))
					{
						System.out.println("Found Duplicate:  " + f);
						s.close();
						s = null;
						f.delete();
						return;
					}
					else
					{
						others.add(data);
					}
					s.close();
					return;
				}
				
				Scanner line = new Scanner(id);
				String result = line.findInLine("\\d+");
				line.close();
				if(ids.contains(result))
				{	
					s.close();
					s = null;
					System.out.println("Found Duplicate:  " + result);
					Files.delete(f.toPath());
				}
				else
				{
					ids.add(result);
					s.close();
					s = null;	
				}	
			}
			catch (Exception e)
			{
				throw new IllegalArgumentException(e);
			}
		});
	}
	

	
	static AtomicLong counter = new AtomicLong();
	public static void unzipFiltered(File file, String targetLocation, byte[] buffer)
	{
		File target = new File(targetLocation);
		target.mkdirs();
		try
		{
			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(file)));
			ZipEntry entry = zis.getNextEntry();
			while(entry != null)
			{
			
				String filename = entry.getName();
				if(filename.endsWith(".txt"))
				{
					File output = new File(targetLocation + counter.incrementAndGet() + ".txt");
					if(output.exists()) 
					{
						System.err.println("Duplicate Filename: Possible concurrency error");
						System.exit(-1);
					}
					output.createNewFile();
					FileOutputStream out = new FileOutputStream(output);
					int len = zis.read(buffer);
			        while (len > 0) 
			        {
			       		out.write(buffer, 0, len);
			       		len = zis.read(buffer);
			        }
			        out.close();
				}
				else
				{
					zis.closeEntry();
				}
				entry = zis.getNextEntry();
			}
			zis.close();
			
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		} 
		
	}
}
