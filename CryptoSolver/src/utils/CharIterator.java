package utils;

import java.util.NoSuchElementException;

public interface CharIterator
{
	CharIterator EMPTY = new CharIterator()
	{

		@Override
		public boolean advance()
		{
			return false;
		}

		@Override
		public char get()
		{
			throw new NoSuchElementException();
		}
		
	};

	public boolean advance();
	
	public char get();
	
	public default char next()
	{
		advance();
		return get();
	}
	
	
}


