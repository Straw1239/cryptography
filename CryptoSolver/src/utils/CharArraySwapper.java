package utils;
import static utils.Utils.*;

public final class CharArraySwapper
{
	private char[] sub;
	private int index1 = 0;
	private int index2 = 0;

	public CharArraySwapper(char[] state)
	{
		sub = state;
	}
	
	public int firstSwapIndex()
	{
		return index1;
	}
	
	public int secondSwapIndex()
	{
		return index2;
	}
	
	public boolean hasNext()
	{
		return !(index1 == sub.length - 2 && index2 == sub.length - 1);
	}
	
	public void advance()
	{
		//if(!hasNext()) throw new IllegalStateException();
		swap(sub, index1, index2); // Swap back from old configuration
		index2++;
		if(index2 == sub.length)
		{
			index1++;
			index2 = index1 + 1;
		}
		swap(sub, index1, index2);
	}
	
	public char[] get()
	{
		return sub;
	}
	
	public void revert()
	{
		if(hasNext()) throw new IllegalStateException();
		swap(sub, index1, index2);
	}
	
}

