package utils;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

public class TabuCache<E> extends LinkedHashMap<E, Boolean>
{
	private static final long serialVersionUID = -2959174925786362049L;
	
	
	private int maxSize;
	
	public TabuCache(int size)
	{
		super(size);
		maxSize = size;
	}
	
	@Override
	protected boolean removeEldestEntry(Map.Entry<E, Boolean> e)
	{
		return size() > maxSize;
	}
	
	public Boolean get(Object arg)
	{
		return super.getOrDefault(arg, false);
	}
	
	public boolean contains(Object arg)
	{
		return get(arg);
	}
	
	public boolean allow(Object arg)
	{
		return !contains(arg);
	}
	
	public void add(E arg)
	{
		put(arg, true);
	}

	public void increaseSize()
	{
		maxSize++;
	}

	public void increaseSize(int i)
	{
		maxSize += i;
	}
	
	
	
}
