package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class SettableSearch<T> implements Search<T>
{
	private volatile T best;
	private volatile boolean done = false;
	private volatile boolean shouldStop = false;
	private volatile List<Consumer<T>> listeners = new ArrayList<>();
	
	public SettableSearch()
	{
		this(null);
	}
	
	public SettableSearch(T value)
	{
		best = value;
	}
	
	@Override
	public T bestSoFar()
	{
		return best;
	}

	@Override
	public boolean isDone()
	{
		return done;
	}

	@Override
	public void stop()
	{
		shouldStop = true;
	}
	
	public boolean shouldStop()
	{
		return shouldStop;
	}
	
	public void setResult(T result)
	{
		best = result;
		for(Consumer<T> c : listeners)
		{
			c.accept(best);
		}
	}

	@Override
	public void addUpdateListener(Consumer<T> listener)
	{
		listeners.add(listener);
	}

}
