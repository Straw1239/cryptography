package utils;

import static english.English.alphabet;
import static english.English.uppercase;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Spliterator;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.Spliterators;
import java.util.concurrent.Executors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

public class Utils
{
	
	/**
	 * Indicates the number of threads or level of parallelism which should be used in calculation.
	 */
	public static final int THREADS = Runtime.getRuntime().availableProcessors();
	
	public static final ListeningScheduledExecutorService compute = MoreExecutors.listeningDecorator(Executors.newScheduledThreadPool(THREADS));
	
	public static final ListeningExecutorService exec = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
	
	public static final Random rand = new ParallelRandom();
	
	public static final char shift(char base, int shift)
	{
		int n = indexOf(base);
		n += shift;
		while(n < 0) n += alphabet.length();
		n %= alphabet.length();
		if(Character.isUpperCase(base)) return uppercase.charAt(n);
		else return alphabet.charAt(n);
	}
	
	public static void swap(byte[] array2, int index12, int index22)
	{
		byte c = array2[index12];
		array2[index12] = array2[index22];
		array2[index22] = c;
		
	}
	
	public static CharIterator flatten(Iterator<? extends CharSequence> source)
	{
		if(!source.hasNext())
		{
			return CharIterator.EMPTY;
		}
		return new CharIterator()
		{
			int index = -1;
			CharSequence current = source.next();
			@Override
			public boolean advance()
			{
				index++;
				while(index == current.length())
				{
					if(!source.hasNext()) return false;
					index = 0;
					current = source.next();
				}
				return true;
			}

			@Override
			public char get()
			{
				return current.charAt(index);
			}
			
		};
	}
	
	public static final int shiftOf(char a, char b)
	{
		throw new UnsupportedOperationException();
	}
	
	public static int lowercaseIndex(char c)
	{
		return c - 97;
	}
	
	public static int uppercaseIndex(char c)
	{
		return c - 65;
	}
	
	public static boolean isUpperAlpha(char c)
	{
		return (c >= 65 & c < 91);
	}
	
	public static boolean isLowerAlpha(char c)
	{
		return (c >= 97 & c < 123);
	}
	
	public static boolean isAlphabetic(char c)
	{
		return isUpperAlpha(c) | isLowerAlpha(c);
	}
	public static int indexOf(char c)
	{
		return Character.isUpperCase(c) ? uppercaseIndex(c) : lowercaseIndex(c);
	}
	
	public static <K, V> Collection<Entry<K, V>> rangeSearch(NavigableMap<K, V> map, K min, K max)
	{
		return map.subMap(min, true, max, true).entrySet();
	}
	
	public static String removePunctuation(String text)
	{
		text = text.toLowerCase();
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if(isLowerAlpha(c)) builder.append(text.charAt(i));
		}
		return builder.toString();
	}
	
	
	
	public static Iterable<Character> stringIterable(String s)
	{
		return Lists.charactersOf(s);
	}
	
	public static Iterator<Character> iterator(String s)
	{
		return stringIterable(s).iterator();
	}
	
	public static void swap(char[] array, int index1, int index2)
	{
		char temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
	}
	
	public static void swap(int[] array, int index1, int index2)
	{
		int temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
	}
	
	public static <T, V> void swap(Map<T, V> map, T a, T b)
	{
		V temp = map.get(a);
		map.put(a, map.get(b));
		map.put(b, temp);
	}
	
	public static <K, V> Entry<K, V> entryFrom(K k, V v)
	{
		return new Entry<K, V>()
		{
			public String toString()
			{
				return "(" + k + ", " + v + ")";
			}
			@Override
			public K getKey()
			{
				return k;
			}

			@Override
			public V getValue()
			{
				return v;
			}

			@Override
			public V setValue(V value)
			{
				throw new UnsupportedOperationException();
			}
			
		};
	}

	public static <T> Stream<T> stream(Iterator<T> itr)
	{
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(itr, 0), false);
	}
	
}
