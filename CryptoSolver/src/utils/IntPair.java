package utils;

public final class IntPair
{
	public final int a, b;
	
	public IntPair(int a, int b)
	{
		this.a = a;
		this.b = b;
	}

	public static IntPair of(int a, int b)
	{
		return new IntPair(a, b);
	}

	@Override
	public String toString()
	{
		return String.format("(%d, %d)", a, b);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + a;
		result = prime * result + b;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (!(obj instanceof IntPair)) return false;
		IntPair other = (IntPair) obj;
		if (a != other.a) return false;
		if (b != other.b) return false;
		return true;
	}
}
