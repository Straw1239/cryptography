package utils;

import java.util.function.Consumer;

public interface Search<T>
{
	public T bestSoFar();
	
	public boolean isDone();
	
	public void stop();
	
	public void addUpdateListener(Consumer<T> listener);
}
